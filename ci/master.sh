mkdir -p ~/.ssh
(umask  077 ; echo $KEY | base64 --decode -i > ~/.ssh/id_rsa)
# pull api source
ssh serverpilot@etl.yt 'cd /srv/users/serverpilot/apps/pss-adresar && git pull origin master && rm-rf ./apidoc/* && cp -r ./apidoc/* ./public/apidoc'
# build project
git clone git@bitbucket.org:eutelnet/pss-adresar-app.git
cd pss-adresar-app
npm install
npm run build
ssh serverpilot@etl.yt 'rm -rf /srv/users/serverpilot/apps/pss-adresar/public/app/*'
scp -r ./build/* serverpilot@etl.yt:/srv/users/serverpilot/apps/pss-adresar/public/app