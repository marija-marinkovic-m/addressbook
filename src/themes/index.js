import { fade } from 'material-ui/utils/colorManipulator';
import spacing from 'material-ui/styles/spacing';
import { red500, fullWhite, fullBlack, grey800, grey700, grey600, redA100, darkBlack, lightBlack, grey500 } from 'material-ui/styles/colors';

export const pssLight = {
  spacing,
  palette: {
    primary1Color: '#f3f3f3',
    primary2Color: red500,
    accent1Color: redA100,
    pickerHeaderColor: red500,
    textColor: darkBlack,
    alternateTextColor: lightBlack
  },
  appBar: {
    color: red500,
    textColor: fullWhite
  },
  flatButton: {
    primaryTextColor: red500
  },
  textField: {
    errorColor: redA100,
    focusColor: lightBlack,
    disabledTextColor: fade(darkBlack, .5),
    floatingLabelColor: fade(darkBlack, .5)
  },
  checkbox: {
    checkedColor: red500,
    requiredColor: redA100,
    labelColor: fade(lightBlack, .6)
  },
  radioButton: {
    borderColor: lightBlack,
    checkedColor: red500,
    requiredColor: redA100,
    labelColor: lightBlack
  },
  datePicker: {
    textColor: fullWhite,
    selectTextColor: fullWhite,
    color: red500
  },
  raisedButton: {
    color: fullWhite,
    secondaryColor: fullWhite 
  },
  toggle: {
    thumbOnColor: red500,
    trackOnColor: fade(red500, 0.5),
    thumbRequiredColor: redA100,
    trackRequiredColor: fade(redA100, 0.5),
    labelColor: fade(lightBlack, .4)
  },
  snackbar: {
    textColor: fullWhite,
    backgroundColor: red500
  }
};
export const pssDark = {
  spacing: spacing,
  borderRadius: 2,
  palette: {
    primary1Color: '#303030',
    primary2Color: red500,
    primary3Color: grey600,
    accent1Color: redA100,
    accent2Color: grey700,
    accent3Color: grey800,
    textColor: fullWhite,
    secondaryTextColor: fade(fullWhite, 0.7),
    alternateTextColor: fullWhite,
    canvasColor: '#303030',
    borderColor: fade(fullWhite, 0.3),
    disabledColor: fade(fullWhite, 0.6),
    pickerHeaderColor: fade(fullWhite, 0.12),
    clockCircleColor: fade(fullWhite, 0.12)
  },
  appBar: {
    color: red500,
    // textColor: palette.alternateTextColor,
    // height: spacing.desktopKeylineIncrement,
    // titleFontWeight: typography.fontWeightNormal,
    // padding: spacing.desktopGutter,
  },
  raisedButton: {
    textColor: fullBlack,
    color: grey500,
    primaryColor: red500
  },
  flatButton: {
    primaryTextColor: red500
  },
  textField: {
    errorColor: redA100,
    focusColor: fade(fullWhite, 0.7)
  },
  radioButton: {
    borderColor: fade(fullWhite, 0.7),
    checkedColor: red500,
    requiredColor: redA100,
    labelColor: fade(fullWhite, 0.7)
  },
  datePicker: {
    textColor: fullWhite,
    selectTextColor: fullWhite,
    color: red500
  },
  checkbox: {
    checkedColor: red500,
    requiredColor: redA100,
    labelColor: fade(fullWhite, .5)
  },
  toggle: {
    thumbOnColor: red500,
    trackOnColor: fade(red500, 0.5),
    thumbRequiredColor: redA100,
    trackRequiredColor: fade(redA100, 0.5),
    labelColor: fade(fullWhite, .5)
  },
  snackbar: {
    backgroundColor: red500
  }
}