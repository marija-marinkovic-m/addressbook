import React from 'react';
import PropTypes from 'prop-types';

export const getExcerpt = (content, maxLength = 40, separator = ' ') => {
  if (content.length <= maxLength) return content;
  return content.substr(0, content.lastIndexOf(separator, maxLength)) + '... ';
}

const ExcerptFieldFn = ({record = {}, source, maxLength}) => (
  <p>{ getExcerpt(record[source], maxLength) }</p>
);
ExcerptFieldFn.propTypes = {
  record: PropTypes.object,
  source: PropTypes.string.isRequired,
  maxLength: PropTypes.number
}
ExcerptFieldFn.defaultProps = {
  maxLength: 40
}

export const ExcerptField = ExcerptFieldFn;