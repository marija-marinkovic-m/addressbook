export * from './storage';
export * from './excerpt';
export * from './moment';


export const cityRenderer = choice => choice && choice.name && `${choice.name}, ${choice.country.name}`;
export const fullNameRenderer = choice => choice && `${choice.first_name} ${choice.last_name}`;
export const clubRenderer = choice => choice && `${choice.name}, ${cityRenderer(choice.city)}`;

export const iStyles = {
  input: { display: 'inline-block', verticalAlign: 'top' },
  inputLeft: { display: 'inline-block', verticalAlign: 'top', marginRight: '32px' }
};