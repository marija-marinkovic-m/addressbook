import * as moment from 'moment';
import 'moment/locale/sr';
import { connect } from 'react-redux';

const FromNowFn = ({date, locale}) => {
    moment.locale(locale);
    return moment(date).fromNow();
}
const MonthsShortFn = ({month, locale}) => {
  moment.locale(locale);
  return moment.monthsShort('-MMM-', month);
}

const mapStateToProps = state => ({
  locale: state.locale
});

export const FromNow = connect(mapStateToProps)(FromNowFn);
export const MonthsShort = connect(mapStateToProps)(MonthsShortFn);