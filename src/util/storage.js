export const storage = {
  save: (token, infoObj) => {
    localStorage.setItem('token', token);
    localStorage.setItem('exp', infoObj.exp);
    localStorage.setItem('rights', infoObj.rights);
    localStorage.setItem('id', infoObj.sub);
    localStorage.setItem('username', infoObj.username);
    localStorage.setItem('email', infoObj.email);
    localStorage.setItem('club_id', infoObj.club_id);

    localStorage.setItem('decd', JSON.stringify(infoObj));
  },
  clear: () => {
    const apiFields = ['token', 'exp', 'rights', 'decd'];
    apiFields.forEach(apiField => {
      localStorage.removeItem(apiField);
    });
  },
  getItem: (key) => localStorage.getItem(key),
  setItem: (prop, value) => localStorage.setItem(prop, value)
};