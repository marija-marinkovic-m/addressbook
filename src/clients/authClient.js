import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_ERROR, AUTH_CHECK, AUTH_GET_PERMISSIONS, fetchUtils } from 'admin-on-rest';
import decodeJwt from 'jwt-decode';

import { storage as storageData } from '../util';

export const authClient = (API_URL, type, params) => {

  // called when the user attempts to log in
  if (type === AUTH_LOGIN) {
    const { email, password } = params;

    return fetchUtils
      .fetchJson(`${API_URL}/login`, {
        method: 'POST', 
        body: JSON.stringify({email, password})
      })
      .then(successRes => {
        if (successRes.json && successRes.json.token) {
          const decodedToken = decodeJwt(successRes.json.token);
          storageData.save(successRes.json.token, decodedToken);
          return Promise.resolve();
        }
        return Promise.reject('aor.auth.missing_token');
      })
      .catch(err => {
        if (err.body && typeof err.body === 'object') {

          if (err.body.hasOwnProperty('email')) return Promise.reject('aor.validation.email');
          if (err.body.hasOwnProperty('password')) return Promise.reject('aor.validation.password');
          if (err.body.hasOwnProperty('error') && typeof err.body.error === 'string') return Promise.reject(`aor.auth.${err.body.error}`);
        }
        return Promise.reject('aor.auth.sign_in_error')
      });
  } 

  // called when the user clicks on the logout button
  if (type === AUTH_LOGOUT) {
    storageData.clear();
    return Promise.resolve();
  }

  // called when the API returns an error
  if (type === AUTH_ERROR) {
    const {status} = params;
    if (status === 401 || status === 403) {
      storageData.clear();
      return Promise.reject('aor.auth.unauthorized');
    }
    return Promise.resolve();
  }

  // called when the user navigates to a new location
  if (type === AUTH_CHECK) {
    const exp = parseInt(storageData.getItem('exp'), 10) * 1000;
    const token = storageData.getItem('token');

    return token && exp && exp > Date.now()
      ? Promise.resolve()
      : Promise.reject();
  }

  if (type === AUTH_GET_PERMISSIONS) {
    const rights = storageData.getItem('rights');
    return Promise.resolve(rights);
  }

  return Promise.reject('Unknown method');
}