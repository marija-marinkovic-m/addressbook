import { stringify } from 'query-string';
import { 
  GET_LIST,
  GET_ONE,
  GET_MANY,
  GET_MANY_REFERENCE,
  CREATE,
  UPDATE,
  DELETE,
  fetchUtils
 } from 'admin-on-rest';

import { storage as storageData } from '../util';
import { GET_ONE_RESOLVE, UPDATE_PROFILE } from '../actions';

export const getAuthorizationData = () => {
  if (!storageData.getItem('token')) return false;

  const exp = parseInt(storageData.getItem('exp'), 10) * 1000;
  const token = storageData.getItem('token');

  return ({
    authenticated: exp && exp > Date.now(),
    token: 'Bearer ' + token
  });
 }

export const UPDATE_CLUB_ADMIN = 'UPDATE_CLUB_ADMIN';

 /**
  * Maps admin-on-rest queries to a pss adresar REST API
  *
  * The REST dialect
  ** @example 
  * GET_LIST    => GET http://api.pss.app/user
  */
export const restClient = (apiUrl, httpClient = fetchUtils.fetchJson) => {
    /**
     * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
     * @param {String} resource Name of the resource to fetch, e.g. 'posts'
     * @param {Object} params The REST request params {pagination: {page: (int), perPage: (int)}, sort: {field: (string), order: (ASC|DESC)}, filter: (Object)}
     * @returns {Object} { url, options } The HTTP request parameters
     */
    const convertRESTRequestToHTTP = (type, resource, params) => {
      let url = '';
      let query = {};
      const options = {};

      // Authorization token
      const authorizationData = getAuthorizationData();
      if (false !== authorizationData) {
        options.user = authorizationData;
      }

      switch (type) {
        case GET_LIST: {
          const {page, perPage} = params.pagination;
          const {field, order} = params.sort;

          if ('membership-fee' === resource && (!params.filter || !params.filter.club_id)) {
            params.filter.club_id = storageData.getItem('club_id') || 1;
          }

          query = Object.assign({}, {
            page,
            perPage,
            sort: `${field}:${order}`
          }, params.filter);

          url = `${apiUrl}/${resource}?${stringify(query, {arrayFormat: 'bracket'})}`;
          break;
        }
        case GET_ONE: {
          url = `${apiUrl}/${resource}/${params.id}`;
          break;
        }
        case GET_ONE_RESOLVE: {
          url = `${apiUrl}/${resource}/${params.id}/resolve`;
          break;
        }
        case GET_MANY: {
          // @TBD: get many not integrated with api
          // query = Object.assign({}, {
          //   filter: JSON.stringify({id: params.ids})
          // });

          let filter = {id: []};

          params.ids.forEach(element => {
            if (isNaN(element)) return;
            filter.id.push(element)
          });

          query = {};

          if (filter.id.length) {
            query.filter = JSON.stringify({ id: filter.id });
          }
          url = `${apiUrl}/${resource}?${stringify(query, {arrayFormat: 'bracket'})}`;
          break;
        }
        case GET_MANY_REFERENCE: {
          query = Object.assign({}, {
            page: params.pagination.page,
            perPage: params.pagination.perPage,
            sort: `${params.sort.field}:${params.sort.order}`,
            [params.target]: params.id
          }, params.filter);
          url = `${apiUrl}/${resource}?${stringify(query, {arrayFormat: 'bracket'})}`;
          break;
        }
        case UPDATE: {
          url = `${apiUrl}/${resource}/${params.id}`;
          options.method = 'PATCH';
          options.body = JSON.stringify(params.data);
          break;
        }
        case UPDATE_CLUB_ADMIN: {
          url = `${apiUrl}/${resource}/${params.data.club_id}/user`;
          options.method = 'PATCH';
          options.body = JSON.stringify(params.data);
          break;
        }
        case UPDATE_PROFILE: {
          url = `${apiUrl}/${resource}`;
          options.method = 'PATCH';
          options.body = JSON.stringify(params.data);
          break;
        }
        case CREATE: {
          url = `${apiUrl}/${resource}`;
          options.method = 'POST';
          options.body = JSON.stringify(params.data);
          break;
        }
        case DELETE: {
          url = `${apiUrl}/${resource}/${params.id}`;
          options.method = 'DELETE';
          break;
        }
        default: 
          throw new Error(`Unsupported fetch action type ${type}`);
      }
      return { url, options };
    }

    /**
      * @param {Object} response HTTP response from fetch()
      * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
      * @param {String} resource Name of the resource to fetch, e.g. 'posts'
      * @param {Object} params The REST request params, depending on the type
      * @returns {Object} REST response
      */
    const convertHTTPResponseToREST = (response, type, resource, params) => {
        const { /* headers, */ json: {data, paginator} } = response;
        switch (type) {
          case GET_LIST:
          case GET_MANY_REFERENCE:
            return {
              data,
              total: (paginator && paginator.total) || data.length
            };
          case CREATE:
            return { data: {...params.data, id: response.json.id, json: response.json} }
          default: 
            return { data };
        }
    }

    /**
     * @param {string} type Request type, e.g GET_LIST
     * @param {string} resource Resource name, e.g. "posts"
     * @param {Object} params Request parameters. Depends on the request type
     * @returns {Promise} the Promise for a REST response
     */
    return (type, resource, params) => {
      const { url, options } = convertRESTRequestToHTTP(
        type,
        resource,
        params
      );
      return httpClient(url, options)
      .then(response => convertHTTPResponseToREST(response, type, resource, params))
    };
}
