import { CHANGE_THEME } from '../actions';
import { storage } from '../util';

import { USER_LOGIN } from 'admin-on-rest';

const defaultTheme = () => storage.getItem('theme') || 'light';

export const themeReducer = (previousState = defaultTheme(), { type, payload }) => {
  if (type === CHANGE_THEME) {
    storage.setItem('theme', payload);
    return payload;
  }
  if (type === USER_LOGIN) {
    return defaultTheme();
  }
  return previousState;
}