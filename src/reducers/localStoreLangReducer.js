import { CHANGE_LOCALE } from 'admin-on-rest';
import { storage } from '../util';



export const localStoreLangReducer = (previousState = storage.getItem('lang') || 'sr', { type, payload }) => {
  if (type === CHANGE_LOCALE) {
    storage.setItem('lang', payload);
    return payload;
  }
  return previousState;
}