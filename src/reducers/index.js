import { themeReducer } from './themeReducer';
import { localStoreLangReducer } from './localStoreLangReducer';
import { fetchNewsReducer } from './fetchNewsReducer';
import { fetchEventsReducer } from './fetchEventsReducer';

export default ({
  theme: themeReducer,
  locale: localStoreLangReducer,
  news: fetchNewsReducer,
  events: fetchEventsReducer
});