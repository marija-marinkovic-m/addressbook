import { fetchUtils } from 'admin-on-rest';
import { START_FETCH_NEWS, END_FETCH_NEWS, UPDATE_NEWS_FEATURED_IMG } from '../actions';

export const fetchNewsReducer = (previousState = [], { type, payload, cb }) => {
  switch(type) {
    case END_FETCH_NEWS:
      return payload;

    case START_FETCH_NEWS:
      fetchUtils.fetchJson(payload.url)
        .then(res => payload.cb(res.json))
        .catch(err => payload.cb([]));
      return [];

    case UPDATE_NEWS_FEATURED_IMG:
      const postIndex = previousState.findIndex(p => p.id === payload.postId);
      if (postIndex < 0) return previousState;

      return [
        ...previousState.slice(0, postIndex),
        Object.assign({}, previousState[postIndex], {
          featuredSrc: payload.featuredSrc
        }),
        ...previousState.slice(postIndex + 1, previousState.length)
      ];
    
    default:
      return previousState;
  }
}