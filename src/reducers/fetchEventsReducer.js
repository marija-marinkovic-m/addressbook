import { fetchUtils } from 'admin-on-rest';
import { START_FETCH_EVENTS, END_FETCH_EVENTS } from '../actions';

export const fetchEventsReducer = (state = [], {type, payload}) => {
  switch(type) {
    case END_FETCH_EVENTS:
      return payload;

    case START_FETCH_EVENTS:
      fetchUtils.fetchJson(payload.url)
        .then(res => payload.cb(res.json.events || res.json))
        .catch(err => payload.cb([]));
      return [];

    default:
      return state;
  }
}