import React from 'react';
import { Route } from 'react-router-dom';

import { Settings, MyProfile } from './components';

export default [
  <Route exact path="/settings" component={Settings} />,
  <Route exact path="/profile" component={MyProfile} />
];