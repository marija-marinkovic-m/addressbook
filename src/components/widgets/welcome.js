import React from 'react';
import { Card } from 'material-ui/Card';

export const WelcomeWidget = ({ title, style }) => (
  <Card style={Object.assign({}, {borderLeft: 'solid 4px #f44236'}, style)}>
    <section className="pss-pageIntro">
      <hgroup>
        <h2>{ title }</h2>
      </hgroup>
    </section>
  </Card>
);