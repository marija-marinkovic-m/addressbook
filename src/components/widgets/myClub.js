import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { TextField, FunctionField, SimpleShowLayout } from 'admin-on-rest';
import { Card, CardActions, CardHeader } from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';

import CheckCircle from 'material-ui/svg-icons/action/check-circle';
import WarnTriangle from 'material-ui/svg-icons/alert/warning';
import { red100, green100 } from 'material-ui/styles/colors';
import Subdirectory from 'material-ui/svg-icons/navigation/subdirectory-arrow-right';
import Edit from 'material-ui/svg-icons/editor/mode-edit';
import { fullNameRenderer } from '../../util/index';

const addressRenderer = (item) => (
  <span>
    <strong>{ item.address }</strong>, {item.city.name}, {item.city.country.name}
  </span>
);

export class MyClubWidget extends Component {
  static propTypes = {
    club: PropTypes.object,
    onView: PropTypes.func,
    onEdit: PropTypes.func
  }
  static defaultProps = {
    club: null
  }

  render() {
    const { club, onView, onEdit } = this.props;
    if (!club || !club.id) return null;

    
    const ActivityButton = () => {
      const isActive = club.active === 'active';
      const activityIcon = !isActive ? <WarnTriangle /> : <CheckCircle />;
      return <IconButton
        tooltip={club.active}
        tooltipPosition="top-center"
        iconStyle={{fill: !isActive ? red100 : green100 }}>
        {activityIcon}
      </IconButton> 
    };
    const TheTitle = () => <h3 style={{margin: 0}}>{club.name}<ActivityButton /></h3>

    return (
      <Card style={styles.card}>
        <CardHeader
          title={<TheTitle />}
          subtitle={addressRenderer(club)}
          actAsExpander={true}
          showExpandableButton={true}
          style={{paddingTop: 0, paddingBottom: 0}} />

        <SimpleShowLayout record={club} resource="club">
          <FunctionField
            style={styles.cols.col}
            source="contact_person_id"
            render={i => i.contact_person ? <Link to={`/user/${i.contact_person.id}/show`}>{fullNameRenderer(i.contact_person)}</Link> : '---'} />
        </SimpleShowLayout>

        <SimpleShowLayout expandable={true} record={club} resource="club">
          <TextField style={styles.cols.col} source="phone_number" />
          <TextField style={styles.cols.col} source="monthly_membership_fee" />
          <TextField source="description" />
        </SimpleShowLayout>

        <CardActions expandable={true} style={styles.actions}>
            { onEdit && <IconButton onClick={onEdit}><Edit /></IconButton>}
            { onView && <IconButton onClick={onView}><Subdirectory /></IconButton> }
        </CardActions>
      </Card>
    );
  }
}

const styles = {
  card: { borderLeft: 'solid 4px #ffc107', flex: 1, marginRight: '1em', marginBottom: '2em' },
  icon: { float: 'right', width: 64, height: 64, color: '#ffc107', padding: 0, lineHeight: '64px', fontSize: '30px', textAlign: 'center' },
  titleLink: { textDecoration: 'none', color: 'inherit' },
  smallIcon: { width: '15px', height: '15px', verticalAlign: 'middle', color: 'inherit' },
  actions: { textAlign: 'right' },
  cols: {
    col: {display: 'inline-block', verticalAlign: 'top', width: '100%'}
  }
};