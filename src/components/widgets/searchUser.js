import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import {
  crudGetList as crudGetListAction,
  translate,
  SimpleForm, TextInput, ReferenceInput,
  ReferenceArrayInput
} from 'admin-on-rest';
import { stringify } from 'query-string';
import { Card, CardHeader } from 'material-ui/Card';

import SearchIcon from 'material-ui/svg-icons/action/search';
import { storage } from '../../util';

import SelectArrayInput from '../inputs/SelectArrayInput';
import AutocompleteInput from '../inputs/AutocompleteInput'


class SearchDialogClass extends Component {
  state = {
    isOpen: false
  }
  static propTypes = {
    containerStyle: PropTypes.object,
    rights: PropTypes.number, // local storage
    history: PropTypes.object, // react-router
    crudGetList: PropTypes.func, // admin-on-rest
    translate: PropTypes.func, // admin-on-rest,
    form: PropTypes.object // admin-on-rest
  }
  static defaultProps = {
    containerStyle: {},
    rights: parseInt(storage.getItem('rights'), 10),
    uID: storage.getItem('sub') || 1,
    resource: 'user'
  }
  handleOpen = () => this.setState({isOpen: true})
  handleClose = () => this.setState({isOpen: false})

  handleSubmit = () => {
    console.log(this.props.form['record-form'].values);
    
    const resource = this.props.resource;
    const pagination = { page: 1, perPage: 10 };
    const sort = { field: 'id', order: 'DESC' };
    const filter = this.props.form['record-form'].values;
    this.props.history.push(`/${resource}?${stringify({
      page: pagination.page,
      perPage: pagination.perPage,
      sort: `${sort.field}:${sort.order}`,
      filter: JSON.stringify(filter)
    })}`);
    this.props.crudGetList(resource, pagination, sort, filter);
  }

  componentDidMount() {

  }

  render() {
    const { translate, containerStyle } = this.props;
    const actions = [
      <FlatButton
        label={translate('misc.searchUsers.fields.cancel')}
        secondary={true}
        onClick={this.handleClose} />,
      <FlatButton
        label={translate('misc.searchUsers.fields.submit')}
        primary={true}
        disabled={false}
        onClick={this.handleSubmit} />
    ];
    return (
      <div>
        <Card style={Object.assign({}, styles.card, containerStyle)}>
          <SearchIcon style={styles.icon} />
          <CardHeader
            title={<RaisedButton
              labelStyle={{color: 'white'}}
              style={{marginBottom: '1em'}}
              backgroundColor="#33691e"
              label={translate('misc.searchUsers.title')}
              onClick={this.handleOpen} />}
            subtitle={translate('misc.searchUsers.cardSubtitle')} />
        </Card>

        <Dialog
          title={translate('misc.searchUsers.title')}
          actions={actions}
          modal={true}
          open={this.state.isOpen}
          autoScrollBodyContent={true}>
          <p>{ translate('misc.searchUsers.subtitle') }</p>
          <SimpleForm
            submitOnEnter={false}
            redirect={false}
            toolbar={null}>

            { this.props.rights > 1 && <ReferenceInput
                resource={this.props.resource}
                record={{id: this.props.uID}}
                reference="club"
                source="club_id"
                filterToQuery={name => ({name})}
                label={translate('misc.searchUsers.fields.club')}
                allowEmpty><AutocompleteInput addField={false} /></ReferenceInput> }

            <TextInput source="name" label="misc.searchUsers.fields.name" elStyle={{ width: '100%' }} />
            <TextInput source="personal_document_number" label="misc.searchUsers.fields.personal_document_number" elStyle={{ width: '100%' }} />
            <TextInput source="pss_card_number" label="misc.searchUsers.fields.pss_card_number" elStyle={{ width: '100%' }} />

            <ReferenceArrayInput
              source="specialty_id"
              filterToQuery={name => ({name})}
              reference="specialty"
              resource={this.props.resource}
              record={{id: this.props.uID}}
              label={translate('misc.searchUsers.fields.specialty_id')}
              allowEmpty>
              <SelectArrayInput elStyle={{ width: '100%' }} addField={false} />
            </ReferenceArrayInput>

            {/* <ReferenceArrayInput
              source="title_id"
              filterToQuery={name => ({name})}
              reference="title"
              resource={this.props.resource}
              record={{id: this.props.uID}}
              label={translate('misc.searchUsers.fields.title_id')}
              allowEmpty>
              <SelectArrayInput addField={false} />
            </ReferenceArrayInput>

            <ReferenceArrayInput
              source="mountaineering_course_id"
              filterToQuery={name => ({name})}
              reference="mountaineering-course"
              resource={this.props.resource}
              record={{id: this.props.uID}}
              label={translate('misc.searchUsers.fields.mountaineering_course_id')}
              allowEmpty>
              <SelectArrayInput addField={false} />
            </ReferenceArrayInput> */}

          </SimpleForm>
        </Dialog>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  form: state.form
});
const mapDispatchToProps = dispatchEvent => ({
  crudGetList: bindActionCreators(crudGetListAction, dispatchEvent)
});

export const SearchDialog = compose(
  withRouter,
  translate,
  connect(mapStateToProps, mapDispatchToProps)
)(SearchDialogClass);


const styles = {
  card: { flex: 1, borderLeft: '4px solid #33691e' },
  icon: { float: 'right', width: 42, height: 42, padding: 16, color: '#33691e' }
}