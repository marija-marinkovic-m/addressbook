import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { updateNewsFeaturedImg } from '../../actions';
import { bindActionCreators } from 'redux';
import { fetchUtils } from 'admin-on-rest';
import { Card, CardTitle, CardActions, CardText } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';

import { FromNow } from '../../util/moment';


import thumb from '../../assets/thumb.png';
import NewTab from 'material-ui/svg-icons/action/open-in-new';

class FeaturedImageClass extends Component {
  static propTypes = {
    post: PropTypes.object.isRequired,
    updatePostImage: PropTypes.func,
    size: PropTypes.string,
    style: PropTypes.object
  }
  static defaultProps = {
    size: 'small',
    style: {margin: 0}
  }
  constructor(props) {
    super(props);
    this.state = {
      imgSrc: this.getFeaturedSrc(props)
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.post.featuredSrc !== this.state.imgSrc) {
      this.setState({imgSrc: this.getFeaturedSrc(nextProps)});
    }
  }
  getFeaturedSrc = (props) => {
    if (props.post.featuredSrc) return props.post.featuredSrc;

    const { post, size, updatePostImage } = props;
    if (
        post.featured_media &&
        post._links &&
        post._links['wp:featuredmedia'] &&
        post._links['wp:featuredmedia'].length &&
        post._links['wp:featuredmedia'][0].embeddable &&
        post._links['wp:featuredmedia'][0].href
      ) {
      fetchUtils.fetchJson(post._links['wp:featuredmedia'][0].href)
        .then(res => {
          if (!res.json || !res.json.media_details || !res.json.media_details.sizes[size]) return;

          const imgSrc = res.json.media_details.sizes[size].source_url;
          updatePostImage(post.id, imgSrc);
        })
        .catch(err => {
          console.log('Image could not be fetched.');
          updatePostImage(post.id, thumb);
        });
    }
    return thumb;
  }
  render() {
    const { imgSrc } = this.state;
    const { post, style } = this.props;
    return (
      <figure style={style}>
        { imgSrc && <img
            src={imgSrc}
            alt={post.title.rendered}
            style={styles.image} /> }
      </figure>
    );
  }
}

const FeaturedImage = connect(undefined, (dispatchEvent) => ({
  updatePostImage: bindActionCreators(
    updateNewsFeaturedImg,
    dispatchEvent
  )
}))(FeaturedImageClass);

export const LatestNews = ({ posts = [], published = 'Published', readMore = 'Read More', allNews = 'All news' }) => {
  if (posts.length < 1) return null;

  const post = posts[0];

  return (
    <Card style={styles.card}>
      <FontIcon className="icon-news" style={styles.icon} />
      <CardTitle
        title={post.title.rendered}
        subtitle={<span>{published}: <FromNow date={post.date_gmt} /></span>} />

      <CardText>
        <FeaturedImage
          post={post}
          style={styles.figure} />
        <div
          style={styles.article}
          dangerouslySetInnerHTML={{__html: post.excerpt.rendered || ''}} />
      </CardText>
      <CardActions style={styles.actions}>
        <FlatButton
          href={post.link}
          target="_blank"
          secondary
          label={readMore}
          icon={<NewTab />} />

        <FlatButton
          href="https://pss.rs/vesti"
          target="_blank"
          label={allNews}
          icon={<NewTab />} />
      </CardActions>
    </Card>
  );
}

const styles = {
  card: { borderLeft: 'solid 4px #f44236', flex: 1, marginRight: '1em' },
  icon: { float: 'right', width: 64, height: 64, color: '#f44236', padding: 0, lineHeight: '64px' },
  figure: { margin: '1em 1em 0.5em 0', float: 'left', width: 62, minHeight: 10 },
  article: { overflow: 'hidden' },
  image: { display: 'block', maxWidth: '100%', height: 'auto' },
  actions: { textAlign: 'right' }
}