import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, CardHeader } from 'material-ui/Card';

const iconHoc = (Icon, color) => (<Icon style={{color, ...styles.icon}} />);

export class TotalWidget extends Component {
  static propTypes = {
    data: PropTypes.shape({
      data: PropTypes.array,
      total: PropTypes.number
    }).isRequired,
    subtitle: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    icon: PropTypes.func.isRequired,
    onClick: PropTypes.func,
    color: PropTypes.string
  }
  static defaultTypes = {
    subtitle: '',
    color: '#ffee58'
  }
  render() {
    const {
      data: {total},
      subtitle,
      icon,
      onClick,
      color
    } = this.props;
    return (
      <Card style={{borderLeft: `solid 4px ${color}`, cursor: onClick ? 'pointer' : 'inherit', ...styles.card}} onClick={onClick}>
        { iconHoc(icon, color) }
        <CardHeader
          title={total}
          subtitle={subtitle}
          titleStyle={styles.title}
          subtitleStyle={styles.subtitle} />
      </Card>
    );
  }
}

const styles = {
  card: { flex: '0 1 0%', flexGrow: 1, marginRight: '1em' },
  icon: { float: 'right', width: 45, height: 45, padding: '8px 16px' },
  title: { fontSize: '28px' },
  subtitle: { textTransform: 'lowercase' }
}