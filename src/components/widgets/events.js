import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, CardTitle, CardText, CardActions } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import NewTab from 'material-ui/svg-icons/action/open-in-new';

import { MonthsShort } from '../../util';

export class EventsWidget extends Component {
  static propTypes = {
    posts: PropTypes.arrayOf(PropTypes.object),
    title: PropTypes.string,
    readMore: PropTypes.string,
    allEvents: PropTypes.string
  }
  render() {
    const { posts, title, subtitle, readMore, allEvents } = this.props;
    return (
      <Card style={styles.card}>
        <FontIcon className="icon-calendar" style={styles.icon} />
        <CardTitle
          title={title}
          subtitle={subtitle} />

        <CardText>
          <ul className="pss-upcomingEvents">
            {
              posts.map(post => {
                return (
                  <li key={post.id}>
                    <div className="ribon">
                      <span className="day">{post.utc_start_date_details.day}</span>
                      <span className="month"><MonthsShort month={post.utc_start_date_details.month} /></span>
                    </div>
                    <div className="info">
                      <h4>{post.title}</h4>
                      <p>
                        <span className="venue">{post.venue.venue}</span>
                        {post.organizer.length > 0 && <span className="organizer" dangerouslySetInnerHTML={{__html: post.organizer[0].organizer}} />}
                      </p>

                      <a href={post.url} target="_blank" className="read-more"><NewTab style={styles.smallIcon} />&nbsp;{readMore}</a>
                    </div>
                  </li>
                );
              })
            }
          </ul>
        </CardText>

        <CardActions style={styles.actions}>
          <FlatButton
            href="https://pss.rs/aktivnosti"
            target="_blank"
            label={allEvents}
            icon={<NewTab />} />
        </CardActions>
      </Card>
    );
  }
}

const styles = {
  card: { borderLeft: 'solid 4px #00bdf7', flex: 1 },
  icon: { float: 'right', width: 64, height: 64, color: '#00bdf7', padding: 0, lineHeight: '64px', fontSize: '30px', textAlign: 'center' },
  titleLink: { textDecoration: 'none', color: 'inherit' },
  smallIcon: { width: '15px', height: '15px', verticalAlign: 'middle', color: 'inherit' },
  actions: { textAlign: 'right' }
};