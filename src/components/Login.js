import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { propTypes, reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';
import compose from 'recompose/compose';

import LocaleSwitcher from './buttons/LocaleSwitcher';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { Card, CardActions } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import TextField from 'material-ui/TextField';
import LockIcon from 'material-ui/svg-icons/action/lock-outline';
import { red500 } from 'material-ui/styles/colors';

import { Notification, translate, userLogin as userLoginAction } from 'admin-on-rest';
import { pssDark, pssLight } from '../themes/index';

import logo from '../logo.png';

import image1 from '../assets/1.jpg';
import image2 from '../assets/2.jpg';
import image3 from '../assets/3.jpg';
import image4 from '../assets/4.jpg';
import image5 from '../assets/5.jpg';
import image6 from '../assets/6.jpg';


const getRandImg = () => {
    const imgArr = [
        image1, image2, image3, image4, image5, image6
    ];
    const randIndex = Math.floor(Math.random() * 6) + 1;
    return imgArr[randIndex];
}

// see http://redux-form.com/6.4.3/examples/material-ui/
const renderInput = ({ meta: { touched, error } = {}, input: { ...inputProps }, ...props }) =>
    <TextField
        errorText={touched && error}
        {...inputProps}
        {...props}
        fullWidth
    />;

class Login extends Component {

    constructor(props) {
        super(props);
        let randImg = getRandImg();
        this.state = {
            img: randImg || image1
        }
    }

    login = ({ email, password }) => {
        const { userLogin, location } = this.props;
        userLogin({ email, password }, location.state ? location.state.nextPathname : '/');
    }

    render() {
        const { handleSubmit, submitting, theme, translate } = this.props;
        const { img } = this.state;

        const muiTheme = getMuiTheme(theme);
        const backgroundImage = `url(${img})`;
        
        return (
            <MuiThemeProvider muiTheme={muiTheme}>
                <div style={{ ...styles.main, backgroundImage }}>
                    <Card className="pss-loginCard" style={styles.card}>
                        <div className="overlay" style={{backgroundImage}} />
                        <div style={styles.avatar}>

                            <figure className="pss-logo">
                                <img src={logo} alt="PSS logo" />
                            </figure>
                            
                        </div>
                        <form onSubmit={handleSubmit(this.login)}>
                            <div style={styles.form}>
                                <div style={styles.input} >
                                    <Field
                                        name="email"
                                        floatingLabelStyle={{color: '#212121'}}
                                        underlineStyle={{borderColor: '#212121'}}
                                        component={renderInput}
                                        floatingLabelText={translate('aor.auth.email')}
                                    />
                                </div>
                                <div style={styles.input}>
                                    <Field
                                        name="password"
                                        component={renderInput}
                                        floatingLabelText={translate('aor.auth.password')}
                                        floatingLabelStyle={{color: '#212121'}}
                                        underlineStyle={{borderColor: '#212121'}}
                                        type="password"
                                    />
                                </div>
                            </div>
                            <CardActions>
                                <RefreshIndicator
                                    style={{ position: 'relative', backgroundColor: 'transparent', display: 'inline-block' }}
                                    loadingColor={red500}
                                    top={0} left={0} status={submitting ? 'loading' : 'hide'} />
                                <RaisedButton
                                    type="submit"
                                    disabled={submitting}
                                    label={translate('aor.auth.sign_in')}
                                    fullWidth
                                    icon={<LockIcon />} />
                            </CardActions>
                        </form>
                    </Card>
                    <LocaleSwitcher style={styles.switcher} />
                    <Notification />
                </div>
            </MuiThemeProvider>
        );
    }
}

Login.propTypes = {
    ...propTypes,
    authClient: PropTypes.func,
    previousRoute: PropTypes.string,
    theme: PropTypes.object.isRequired,
    translate: PropTypes.func.isRequired,
};

Login.defaultProps = {
    theme: {},
};

const mapStateToProps = state => ({
    theme: state.theme === 'dark' ? pssDark : pssLight
});

const enhance = compose(
    translate,
    reduxForm({
        form: 'signIn',
        validate: (values, props) => {
            const errors = {};
            const { translate } = props;
            if (!values.email) errors.email = translate('aor.validation.required');
            if (!values.password) errors.password = translate('aor.validation.required');
            return errors;
        },
    }),
    connect(mapStateToProps, { userLogin: userLoginAction }),
);

export const LoginPage = enhance(Login);

const styles = {
    main: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#212121',
        backgroundPosition: 'center center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundAttachment: 'fixed'
    },
    card: {
        minWidth: 300,
        backgroundColor: 'hsla(0,0%,100%,.3)',
        boxShadow: '5px 3px 30px rgba(0, 0, 0, 0.43)'
    },
    form: {
        padding: '0 1em 1em 1em',
    },
    input: {
        display: 'flex',
    },
    switcher: {
        position: 'fixed',
        bottom: 0,
        right: 0
    }
};