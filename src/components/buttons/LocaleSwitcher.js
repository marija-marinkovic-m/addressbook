import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import { changeLocale as changeLocaleAction } from 'admin-on-rest';
import { bindActionCreators } from 'redux';

const languages = [
  'en',
  'sr'
];

class LocaleSwitcher extends Component {
  static propTypes = {
    locale: PropTypes.string,
    style: PropTypes.object
  }
  static defaultProps = {
    style: {position: 'relative'}
  }

  handleSwitch = lang => this.props.changeLocale(lang);

  render() {
    const { locale, style } = this.props;
    return (
      <div style={style}>
        {
          languages.map(l => {
            const backgroundColor = l === locale ? 'rgba(255,255,255,.5)' : 'rgba(255,255,255,.2)';
            return <RaisedButton
              key={l}
              label={l}
              onClick={this.handleSwitch.bind(null, l)}
              backgroundColor={backgroundColor}
              style={{backgroundColor, minWidth: '40px', borderRadius: 0}}
              buttonStyle={{borderRadius: 0, height: '30px', lineHeight: '30px'}}
              labelStyle={{fontSize: '12px'}} />
          })
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  locale: state.locale
});
const mapDispatchToProps = dispatchEvent => ({
  changeLocale: bindActionCreators(changeLocaleAction, dispatchEvent)
});

export default connect(mapStateToProps, mapDispatchToProps)(LocaleSwitcher);