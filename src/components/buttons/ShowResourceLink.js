import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import FlatButton from 'material-ui/FlatButton';

import VisibilityIcon from 'material-ui/svg-icons/action/visibility';

export class ShowLinkField extends Component {
  static propTypes = {
    record: PropTypes.object,
    source: PropTypes.string,
    resource: PropTypes.string,
    name: PropTypes.string
  }
  static defaultProps = {
    source: 'name',
    id: 'id'
  }
  render() {
    const { resource, record, source, id } = this.props;
    return (
      <FlatButton
        label={'first_name' !== source ? record[source] : `${record.first_name} ${record.last_name}`}
        labelStyle={styles.label}
        style={styles.button}
        icon={<VisibilityIcon style={{opacity: .15}} />}
        containerElement={<Link to={`/${resource}/${record[id]}/show`} />} />
    );
  }
}

const styles = {
  button: { textAlign: 'left' },
  label: { textTransform: 'none' }
};