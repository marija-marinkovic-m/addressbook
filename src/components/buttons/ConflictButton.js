import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import IconButton from 'material-ui/IconButton';

import { translate } from 'admin-on-rest';
import ThumbDown from 'material-ui/svg-icons/action/thumb-down';
import ThumbUp from 'material-ui/svg-icons/action/thumb-up';
import { red500 } from 'material-ui/styles/colors';

import {
  userApprove as userApproveAction,
  userReject as userRejectAction
} from '../../actions'
import { bindActionCreators } from 'redux';
import compose from 'recompose/compose';


class ConflictButtonClass extends Component {
  static propTypes = {
    approve: PropTypes.bool,
    userApprove: PropTypes.func.isRequired,
    userReject: PropTypes.func.isRequired,
    translate: PropTypes.func.isRequired,
    id: PropTypes.number,
    record: PropTypes.object,
    label: PropTypes.string
  }
  static defaultProps = {
    approve: true,
    id: 0
  }
  render() {
    const { approve, translate, userApprove, userReject, id, record, label } = this.props;

    const recordID = record && record.id ? record.id : id;
    const buttonProps = {
      onClick: approve ? userApprove.bind(null, recordID) : userReject.bind(null, recordID),
      hoveredStyle: {backgroundColor: red500}
    };

    if (!label) {
      buttonProps.tooltip = translate(`resources.user.fields.${approve ? 'resolve' : 'remove'}`);
    }

    return (
      <IconButton {...buttonProps}>
        { approve ? <ThumbUp /> : <ThumbDown /> }
      </IconButton>
    );
  }
}

export const ConflictButton = compose(
  translate,
  connect(null, dispatchEvent => ({
    userApprove: bindActionCreators(userApproveAction, dispatchEvent),
    userReject: bindActionCreators(userRejectAction, dispatchEvent)
  }))
)(ConflictButtonClass);

