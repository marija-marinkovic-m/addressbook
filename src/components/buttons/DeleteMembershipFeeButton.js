import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import compose from 'recompose/compose';
import { bindActionCreators } from 'redux';
import IconButton from 'material-ui/IconButton';

import DeleteForever from 'material-ui/svg-icons/action/delete-forever';

import { deleteMembershipFee as deleteMembershipFeeAction } from '../../actions';
import { red500 } from 'material-ui/styles/colors';

class DeleteMembershipFeeButtonClass extends Component {
  static propTypes = {
    deleteMembershipFee: PropTypes.func,
    record: PropTypes.object.isRequired,
    basePath: PropTypes.string.isRequired,
    redirectPath: PropTypes.string
  }

  static defaultProps = {
    redirectPath: ''
  }

  render() {
    const { deleteMembershipFee, record, basePath, redirectPath } = this.props;

    let redirectTo = redirectPath || basePath;

    if ('/show' === redirectTo.slice(-5)) {
      redirectTo = redirectTo.slice(0, -5);
    }

    return (
      <IconButton
        onClick={deleteMembershipFee.bind(null, record.id, redirectTo)}
        hoveredStyle={{backgroundColor: red500}}>
        <DeleteForever />
      </IconButton>
    );
  }
}

export const DeleteMembershipFeeButton = connect(null, (dispatchEvent) => ({
  deleteMembershipFee: bindActionCreators(deleteMembershipFeeAction, dispatchEvent)
}))(DeleteMembershipFeeButtonClass);