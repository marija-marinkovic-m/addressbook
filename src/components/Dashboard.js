import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  fetchNews as fetchNewsAction,
  newsFetched as newsFetchedAction,
  fetchEvents as fetchEventsAction,
  eventsFetched as eventsFetchedAction
} from '../actions';
import { NEWS_URL, EVENTS_URL, API_URL } from '../config';
import {
  LatestNews,
  EventsWidget,
  WelcomeWidget,
  TotalWidget,
  SearchDialog,
  MyClubWidget
} from './widgets';
import { translate, AppBarMobile, GET_LIST, GET_ONE } from 'admin-on-rest';
import withWidth from 'material-ui/utils/withWidth';
import { restClient } from '../clients';
import { storage } from '../util';


import PersonIcon from 'material-ui/svg-icons/social/person-outline';
import ClubIcon from 'material-ui/svg-icons/social/people-outline';
import UnresolvedIcon from 'material-ui/svg-icons/social/person-add';

class DashboardClass extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: { data: [], total: 0 },
      clubs: { data: [], total: 0 },
      unresolved: { data: [], total: 0 },
      club: { data: null },
      rights: parseInt(storage.getItem('rights'), 10)
    }
    // fetch only once
    if (!props.news || !props.news.length) {
      props.fetchNews({url: NEWS_URL, cb: props.newsFetched});
    }
    // @TBD: style events
    if (!props.events || !props.events.length) {
      props.fetchEvents({url: EVENTS_URL, cb: props.eventsFetched});
    }
  }

  componentDidMount() {
    const rClient = restClient(API_URL);

    // myClub (regular + admin + head_admin)
    rClient(GET_ONE, 'club', {
        id: parseInt(storage.getItem('club_id'), 10)
      })
      .then(club => this.setState({club}));

    if (this.state.rights < 1) return;

    // user list (admin + head_admin)
    rClient(GET_LIST, 'user', {
        sort: { field: 'id', order: 'DESC' },
        pagination: { page: 1, perPage: 1 },
        filter: {}
      })
      .then(users => this.setState({users}));

    // club list (admin + head_admin)
    rClient(GET_LIST, 'club', {
        sort: { field: 'id', order: 'DESC' },
        pagination: { page: 1, perPage: 1 },
        filter: {}
      })
      .then(clubs => this.setState({clubs}));

    if (this.state.rights < 2) return;
    // unresolved (head_admin)
    rClient(GET_LIST, 'user/unresolved', {
        sort: { field: 'id', order: 'DESC' },
        pagination: { page: 1, perPage: 5 },
        filter: {}
      })
      .then(unresolved => this.setState({unresolved}));
  }

  changeLocation = (url) => this.props.history.push(url)

  getStats = ({users, clubs, unresolved, rights, translate}) => ({
    Users: () => (users.total > 0 && <TotalWidget
      data={users}
      subtitle={translate('misc.users')}
      icon={PersonIcon}
      onClick={this.changeLocation.bind(null, '/user')}
      color="#ffee58" />),
    Clubs: () => (clubs.total > 0 && <TotalWidget
      data={clubs}
      subtitle={translate('misc.clubs')}
      icon={ClubIcon}
      onClick={this.changeLocation.bind(null, '/club')}
      color="#bbdefb" />),
    Unresolved: () => (rights > 1 && <TotalWidget
      data={unresolved}
      subtitle={translate('misc.unresolvedConflicts')}
      icon={UnresolvedIcon}
      color={unresolved.total > 0 ? '#d50000' : '#9e9e9e'}
      onClick={this.changeLocation.bind(null, '/user/unresolved')} />)
  });

  render() {
    const { news, events, translate, width } = this.props;
    const { rights, club } = this.state;

    const wrap = (Widget) => ((props) => (<div style={styles.flex}><Widget {...props} /></div>));

    const News = () => (news.length > 0 && wrap(LatestNews)({
      posts: news,
      readMore: translate('misc.readMore'),
      published: translate('misc.published'),
      allNews: translate('misc.news')
    }));
    const Events = () => (events.length > 0 && wrap(EventsWidget)({
      posts: events,
      title: translate('misc.calendar'),
      readMore: translate('misc.readMore'),
      allEvents: translate('misc.allEvents')
    }));

    const Stats = () => {
      // stats are not available for regular users
      if (rights < 1) return null;

      const TheStats = this.getStats({...this.state, ...this.props});
      return (
        <div style={styles.flex}>
          <TheStats.Unresolved />
          <TheStats.Users />
          <TheStats.Clubs />
        </div>
      )
    };

    const Club = () => {
      if (!club || !club.data) return null;
      return <MyClubWidget
        club={club.data}
        onView={this.changeLocation.bind(null, `/club/${club.data.id}/show`)}
        onEdit={rights > 0 && this.changeLocation.bind(null, `/club/${club.data.id}`)} />
    }

    return (
      <div>
        {
          width === 1 && 
          <AppBarMobile title={translate('pos.brandName')} />
        }
        <WelcomeWidget
          title={translate('pos.brandName')}
          style={styles.welcome} />

        { rights > 0 && <SearchDialog containerStyle={{marginBottom: '2em'}} /> }
        
        <div style={styles.flex}>
          <div style={styles.leftCol}>
            <Stats />
            <Club />
            <News />
          </div>
          <div style={styles.rightCol}>
            <Events />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  news: state.news,
  events: state.events
});
const mapDispatchToProps = dispatchEvent => ({
  fetchNews: bindActionCreators(fetchNewsAction, dispatchEvent),
  newsFetched: bindActionCreators(newsFetchedAction, dispatchEvent),
  fetchEvents: bindActionCreators(fetchEventsAction, dispatchEvent),
  eventsFetched: bindActionCreators(eventsFetchedAction, dispatchEvent)
});

export const Dashboard = withRouter(withWidth()(
  translate(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )(DashboardClass)
  )
));

const styles = {
  welcome: { marginBottom: '2em' },
  flex: { display: 'flex', marginBottom: '2em' },
  leftCol: { flex: 1, marginRight: '1em' },
  rightCol: { flex: 1, marginLeft: '1em' }
}