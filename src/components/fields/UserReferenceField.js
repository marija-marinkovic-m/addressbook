import React from 'react';
import { ReferenceField, FunctionField } from 'admin-on-rest';
import { fullNameRenderer } from '../../util';

export const UserReferenceField = (props) => {
  if (!props.record || !props.record.contact_person_id) return '---';
  return (
    <ReferenceField {...props}>
      <FunctionField render={(r, s) => r && fullNameRenderer(r)} />
    </ReferenceField>
  );
}