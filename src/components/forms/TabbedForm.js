import React, { Children, Component } from 'react';
import PropTypes from 'prop-types';
import {
  reduxForm,
  getFormAsyncErrors,
  getFormSyncErrors,
  getFormSubmitErrors,
} from 'redux-form';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import { Tabs, Tab } from 'material-ui/Tabs';
import muiThemeable from 'material-ui/styles/muiThemeable';

import { Toolbar } from 'admin-on-rest';

import { createSelector } from 'reselect';

const getDefaultValues = (data = {}, defaultValue = {}, defaultValues = {}) => {
  const globalDefaultValue =
    typeof defaultValue === 'function' ? defaultValue() : defaultValue;
  return { ...globalDefaultValue, ...defaultValues, ...data };
};

const getRecord = (state, props) => props.record;
const getDefaultValue = (state, props) => props.defaultValue;
const getDefaultValuesFromState = state => state.admin.record;

const getDefaultValuesSelector = createSelector(
  getRecord,
  getDefaultValue,
  getDefaultValuesFromState,
  (record, defaultValue, defaultValues) =>
    getDefaultValues(record, defaultValue, defaultValues)
);

const getStyles = theme => ({
  form: { padding: '0' },
  // TODO: The color will be taken from another property in MUI 0.19 and later
  errorTabButton: { color: theme.textField.errorColor },
});

export class TabbedForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
    };
  }

  handleChange = value => {
    this.setState({ value });
  };

  handleSubmitWithRedirect = (redirect = this.props.redirect) =>
    this.props.handleSubmit(values => this.props.save(values, redirect));

  render() {
    const {
            basePath,
      children,
      contentContainerStyle,
      invalid,
      muiTheme,
      record,
      resource,
      submitOnEnter,
      tabsWithErrors,
      toolbar,
      translate,
      version,
      theme
        } = this.props;

    const styles = getStyles(muiTheme);
    
    contentContainerStyle.backgroundColor = 'dark' === theme ? '#292929' : '#f9f9f9';

    return (
      <form className="tabbed-form">
        <div style={styles.form} key={version}>
          <Tabs
            value={this.state.value}
            onChange={this.handleChange}
            contentContainerStyle={contentContainerStyle}
          >
            {React.Children.map(
              children,
              (tab, index) =>
                tab ? (
                  <Tab
                    key={tab.props.label}
                    className="form-tab"
                    label={translate(tab.props.label, {
                      _: tab.props.label,
                    })}
                    value={index}
                    icon={tab.props.icon}
                    buttonStyle={
                      tabsWithErrors.includes(
                        tab.props.label
                      ) && this.state.value !== index ? (
                          styles.errorTabButton
                        ) : null
                    }
                  >
                    {React.cloneElement(tab, {
                      resource,
                      record,
                      basePath,
                    })}
                  </Tab>
                ) : null
            )}
          </Tabs>
        </div>
        {toolbar &&
          React.cloneElement(toolbar, {
            handleSubmitWithRedirect: this.handleSubmitWithRedirect,
            invalid,
            submitOnEnter,
          })}
      </form>
    );
  }
}

TabbedForm.propTypes = {
  basePath: PropTypes.string,
  children: PropTypes.node,
  contentContainerStyle: PropTypes.object,
  defaultValue: PropTypes.oneOfType([PropTypes.object, PropTypes.func]),
  handleSubmit: PropTypes.func, // passed by redux-form
  invalid: PropTypes.bool,
  muiTheme: PropTypes.object.isRequired,
  record: PropTypes.object,
  redirect: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  resource: PropTypes.string,
  save: PropTypes.func, // the handler defined in the parent, which triggers the REST submission
  submitOnEnter: PropTypes.bool,
  tabsWithErrors: PropTypes.arrayOf(PropTypes.string),
  toolbar: PropTypes.element,
  translate: PropTypes.func,
  validate: PropTypes.func,
  version: PropTypes.number,
  theme: PropTypes.string
};

TabbedForm.defaultProps = {
  contentContainerStyle: { padding: '0 1em 45px', borderTop: 'solid 1px #ff8a80' },
  submitOnEnter: true,
  toolbar: <Toolbar />,
};

const collectErrors = state => {
  const syncErrors = getFormSyncErrors('record-form')(state);
  const asyncErrors = getFormAsyncErrors('record-form')(state);
  const submitErrors = getFormSubmitErrors('record-form')(state);

  return {
    ...syncErrors,
    ...asyncErrors,
    ...submitErrors,
  };
};

export const findTabsWithErrors = (
  state,
  props,
  collectErrorsImpl = collectErrors
) => {
  const errors = collectErrorsImpl(state);

  return Children.toArray(props.children).reduce((acc, child) => {
    const inputs = Children.toArray(child.props.children);

    if (inputs.some(input => errors[input.props.source])) {
      return [...acc, child.props.label];
    }

    return acc;
  }, []);
};

const enhance = compose(
  connect((state, props) => {
    const children = Children.toArray(props.children).reduce(
      (acc, child) => [...acc, ...Children.toArray(child.props.children)],
      []
    );

    return {
      tabsWithErrors: findTabsWithErrors(state, props),
      initialValues: getDefaultValuesSelector(state, { ...props, children }),
      theme: state.theme
    };
  }),
  reduxForm({
    form: 'record-form',
    enableReinitialize: true,
  }),
  muiThemeable()
);

export default enhance(TabbedForm);
