import React, { createElement, Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import { Link } from 'react-router-dom';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import CircularProgress from 'material-ui/CircularProgress';

import { pssLight, pssDark } from '../themes';

import withWidth from 'material-ui/utils/withWidth';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import autoprefixer from 'material-ui/utils/autoprefixer';
import Avatar from 'material-ui/Avatar';

import { storage } from '../util';

import {
  AdminRoutes,
  AppBar,
  Menu,
  Notification,
  Sidebar,
  translate,
  setSidebarVisibility as setSidebarVisibilityAction
} from 'admin-on-rest';

// import logo from '../logo.png';
import { grey800 } from 'material-ui/styles/colors';
import { ListItem } from 'material-ui/List';

const componentPropType = PropTypes.oneOfType([
  PropTypes.func,
  PropTypes.string
]);

const prefixedStyles = {};

class LayoutClass extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([PropTypes.func, PropTypes.node]),
    catchAll: componentPropType,
    theme: PropTypes.object.isRequired,
    themeKey: PropTypes.string,
    authClient: PropTypes.func,
    customRoutes: PropTypes.array,
    dashboard: componentPropType,
    isLoading: PropTypes.bool.isRequired,
    menu: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
    resources: PropTypes.array,
    setSidebarVisibility: PropTypes.func.isRequired,
    title: PropTypes.node.isRequired,
    width: PropTypes.number,
    logout: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.func,
      PropTypes.string
    ])
  }
  constructor(props) {
    super(props);
    this.state = {
      username: storage.getItem('username') || 'johndoe',
      email: storage.getItem('email') || 'john@doe.com'
    }
    this.props.setSidebarVisibility(true);
  }
  render() {
    const {
      children,
      customRoutes,
      dashboard,
      isLoading,
      logout,
      menu,
      title,
      theme, themeKey,
      width,
      catchAll
    } = this.props;

    const muiTheme = getMuiTheme(theme);
    if (!prefixedStyles.main) {
      const prefix = autoprefixer(muiTheme);
      Object.keys(styles).map(k => {
        prefixedStyles[k] = prefix(styles[k]);
        return k;
      });
    }

    const bodyStyle = width === 1
      ? prefixedStyles.bodySmall
      : Object.assign({}, prefixedStyles.body, {backgroundColor: themeKey === 'dark' ? grey800 : '#f3f3f3'});

    const contentStyle = width === 1
      ? prefixedStyles.contentSmall
      : prefixedStyles.content;

    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <div style={prefixedStyles.wrapper}>
          <div style={prefixedStyles.main}>
            {width !== 1 && <AppBar
              title={<Link to="/" style={{color: 'inherit', textDecoration: 'none'}}>
              {this.props.translate(title)}
            </Link>} />}
            <div className="body" style={bodyStyle}>
              <div style={contentStyle}>
                <AdminRoutes
                  customRoutes={customRoutes}
                  dashboard={dashboard}
                  catchAll={catchAll}>
                  { children }
                </AdminRoutes>
              </div>
              <Sidebar theme={theme}>
                { createElement(menu || Menu, {
                  logout,
                  hasDashboard: !!dashboard
                }) }
              </Sidebar>
            </div>
            <Notification />
            { isLoading && <CircularProgress
              color="#FFF"
              size={30}
              thickness={2}
              style={prefixedStyles.loader} /> }
    
            <div className="pss-avatarTop">
              <ListItem
                innerDivStyle={styles.avatarTop.innerDivStyle}
                primaryText={this.state.username}
                secondaryText={<div style={styles.avatarTop.secondaryText}>{ this.state.email }</div>}
                rightAvatar={<Avatar backgroundColor="#00bdf7">{this.state.username.charAt(0).toUpperCase()}</Avatar>}
                containerElement={<Link to="/profile">Go to Profile</Link>} />
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

const mapStateToProps = (state) => ({
  isLoading: state.admin.loading > 0,
  theme: state.theme === 'dark' ? pssDark : pssLight,
  themeKey: state.theme
});
const mapDispatchToProps = (dispatchEvent) => ({
  setSidebarVisibility: bindActionCreators(setSidebarVisibilityAction, dispatchEvent)
});

export const Layout = compose(
  translate,
  connect(mapStateToProps, mapDispatchToProps),
  withWidth()
)(LayoutClass);

const styles = {
  wrapper: {
    display: 'flex',
    flexDirection: 'column'
  },
  main: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh'
  },
  body: {
    display: 'flex',
    flex: 1,
    overflowY: 'hidden',
    overflowX: 'scroll'
  },
  bodySmall: {
    backgroundColor: '#FFF'
  },
  content: {
    flex: 1,
    padding: '2em'
  },
  contentSmall: {
    flex: 1,
    paddingTop: '3em'
  },
  loader: {
    position: 'absolute',
    top: 0,
    right: 0,
    margin: 16,
    zIndex: 1200
  },
  avatarTop: {
    innerDivStyle: {
      textAlign: 'right', color: '#fff',
      paddingRight: '70px',
      backgroundColor: 'rgba(0,0,0,.1)',
    },
    secondaryText: {
      height: '16px',
      margin: '4px 0 0',
      fontSize: '14px', lineHeight: '16px', color: 'rgba(255,255,255,.5)',
      overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap'
    }
  }
}
