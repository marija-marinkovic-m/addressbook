import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link, NavLink } from 'react-router-dom';

import { List, ListItem } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';

// icons
import PeopleOutline from 'material-ui/svg-icons/social/people-outline';
import PermIdentity from 'material-ui/svg-icons/action/perm-identity';
import Settings from 'material-ui/svg-icons/action/settings';
import Folder from 'material-ui/svg-icons/file/folder';
import FolderOpen from 'material-ui/svg-icons/file/folder-open';
import Fingerprint from 'material-ui/svg-icons/action/fingerprint';
import Dashboard from 'material-ui/svg-icons/action/dashboard';
import CardMembership from 'material-ui/svg-icons/action/card-membership';


import { yellow400, blue100, brown100 } from 'material-ui/styles/colors';
import { translate } from 'admin-on-rest/lib/i18n';

import { nestedAdministrationLinks } from '../config';
import { storage } from '../util/index';


class CustomMenuClass extends Component {
  static propTypes = {
    pathname: PropTypes.string // redux state (dom location)
  }
  constructor(props) {
    super(props);
    this.state = {
      isSuperAdmin: parseInt(storage.getItem('rights'), 10) === 2,
      isRegular: parseInt(storage.getItem('rights'), 10) === 0
    }
  }
  render() {
    const { onMenuTap, logout, translate } = this.props;
    const { isSuperAdmin, isRegular } = this.state;
    return (
      <List className="pna-sidebarNav">
        <Subheader>{translate('pos.brandName')}</Subheader>
        <NavLink to="/" exact={true}>
          <ListItem
            primaryText={translate('aor.page.dashboard')}
            onTouchTap={onMenuTap}
            leftAvatar={<Avatar icon={<Dashboard />} />} />
        </NavLink>
        <NavLink to="/user" exact={true}>
          <ListItem
            primaryText={translate('resources.user.name', { smart_count: 2 })}
            onTouchTap={onMenuTap}
            leftAvatar={<Avatar icon={<PermIdentity />} backgroundColor={yellow400} />} />
        </NavLink>
        { isSuperAdmin && <NavLink to="/club" exact={true}>
          <ListItem
            primaryText={translate('resources.club.name', { smart_count: 2 })}
            onTouchTap={onMenuTap}
            leftAvatar={<Avatar icon={<PeopleOutline />} backgroundColor={blue100} />} />
        </NavLink> }

        { !isRegular && <NavLink to={`/membership-fee?filter=${encodeURI(JSON.stringify({club_id: storage.getItem('club_id')}))}&page=1&perPage=25`}>
          <ListItem
            primaryText={translate('resources.membership-fee.name', { smart_count: 2 })}
            onTouchTap={onMenuTap}
            leftAvatar={<Avatar icon={<CardMembership />} backgroundColor={brown100} />} />
        </NavLink> }


        { isSuperAdmin && <Divider /> }

        { isSuperAdmin && <Subheader>{translate('misc.otherResources')}</Subheader> }
        
        { 
          isSuperAdmin && (() => {
            const initiallyOpen = nestedAdministrationLinks.map(l => l.linkTo).indexOf(this.props.pathname) > -1;
            return (<ListItem
              initiallyOpen={initiallyOpen}
              primaryText={translate('misc.administration')}
              leftIcon={<Folder />}
              nestedItems={
                nestedAdministrationLinks.map(i => (
                    <ListItem
                      key={i.key}
                      primaryText={translate(i.primaryText, { smart_count: 2 })}
                      onTouchTap={onMenuTap}
                      leftIcon={<FolderOpen />}
                      containerElement={<Link className={this.props.pathname === i.linkTo ? 'active' : 'inactive'} to={i.linkTo} />} />
                ))
              } />);
            })()
          }

        

        <Divider />
        <Subheader>{translate('misc.settings')}</Subheader>

        <NavLink to="/settings">
          <ListItem
            primaryText={translate('pos.configuration')}
            leftIcon={<Settings />} />
        </NavLink>
        <NavLink to="/profile">
          <ListItem
            primaryText={translate('misc.myProfile')}
            leftIcon={<Fingerprint />} />
        </NavLink>
        {logout}
      </List>
    );
  }
}

const mapStateToProps = state => ({
  pathname: state.routing && state.routing.location && state.routing.location.pathname
});
export const CustomMenu = compose(
  translate,
  connect(mapStateToProps)
)(CustomMenuClass);