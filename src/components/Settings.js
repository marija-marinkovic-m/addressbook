import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { translate, changeLocale as changeLocaleAction, ViewTitle } from 'admin-on-rest';
import { changeTheme as changeThemeAction } from '../actions';

import RaisedButton from 'material-ui/RaisedButton';
import { Card, CardText } from 'material-ui/Card';
import { bindActionCreators } from 'redux';

const themes = [
  {
    label: 'pos.theme.dark',
    key: 'dark'
  },
  {
    label: 'pos.theme.light',
    key: 'light'
  }
];

const languages = [
  {
    label: 'en',
    key: 'en'
  },
  {
    label: 'sr',
    key: 'sr'
  }
]

class SettingsClass extends Component {
  static propTypes = {
    theme: PropTypes.string,
    locale: PropTypes.string,
    changeTheme: PropTypes.func,
    changeLocale: PropTypes.func,
    translate: PropTypes.func
  }
  handleChangeTheme = (theme) => this.props.changeTheme(theme);
  handleChangeLocale = (locale) => this.props.changeLocale(locale);

  render() {
    const { theme, locale, translate } = this.props;
    return (
    <Card>
      <ViewTitle title={translate('pos.configuration')} />
      <CardText>
        <div style={styles.label}>{ translate('pos.theme.name') }</div>

        {
          themes.map(t => <RaisedButton
              key={t.key}
              label={translate(t.label)}
              primary={t.key === theme}
              style={styles.button}
              onClick={this.handleChangeTheme.bind(null, t.key)} />)
        }
        
      </CardText>

      <CardText>
        <div style={styles.label}>{ translate('pos.language') }</div>
        
        {
          languages.map(l => <RaisedButton
            key={l.key}
            label={l.label}
            primary={l.key === locale}
            style={styles.button}
            onClick={this.handleChangeLocale.bind(null, l.key)} />)
        }

      </CardText>
    </Card>
    )
  }
}

const mapStateToProps = state => ({
  theme: state.theme,
  locale: state.locale
});
const mapDispatchToProps = dispatchEvent => ({
  changeLocale: bindActionCreators(changeLocaleAction, dispatchEvent),
  changeTheme: bindActionCreators(changeThemeAction, dispatchEvent)
});

export const Settings = connect(mapStateToProps, mapDispatchToProps)(translate(SettingsClass));

const styles = {
  label: { width: '10em', display: 'inline-block' },
  button: { margin: '1em' },
};

