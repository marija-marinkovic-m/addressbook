import React, { Component } from 'react';
import PropTypes from 'prop-types';
import get from 'lodash.get';
import Checkbox from 'material-ui/Checkbox';
import muiThemeable from 'material-ui/styles/muiThemeable';
import compose from 'recompose/compose';

import { FieldTitle, translate, TextInput, required } from 'admin-on-rest';

import { Field } from 'redux-form';

const getStyles = muiTheme => {
    const {
        baseTheme,
        textField: { floatingLabelColor, backgroundColor },
    } = muiTheme;

    return {
        labelContainer: {
            fontSize: 16,
            lineHeight: '24px',
            display: 'inline-block',
            position: 'relative',
            backgroundColor,
            fontFamily: baseTheme.fontFamily,
            cursor: 'auto',
            marginTop: 14,
        },
        label: {
            color: floatingLabelColor,
            lineHeight: '22px',
            zIndex: 1,
            transform: 'scale(0.75)',
            transformOrigin: 'left top',
            pointerEvents: 'none',
            userSelect: 'none',
        },
    };
};

const styles = {
    subFieldsWrap: {},
    subField: { marginRight: '20px', display: 'inline-block', verticalAlign: 'top' }
}

class RenderTextInput extends TextInput {
    componentDidMount() {
        if (this.props.defaultValue && this.props.input.value === '') {
            this.props.input.onChange(this.props.defaultValue);
        }
    }
}

export class CustomCheckboxGroupInputComponent extends Component {
    state = {
        selectedValues: null
    }
    componentWillMount() {
        this.valuesSetup(this.props);
    }
    componentDidUpdate(prevProps, prevState) {
        // here we need to double check if/should selectedValues match props.input.value (strange production #bug)
        if (this.state.selectedValues && this.state.selectedValues.length === this.props.input.value.length && JSON.stringify(this.props.input.value) !== JSON.stringify(this.state.selectedValues)) {
            this.props.input.onChange(this.state.selectedValues);
        }
    }
    valuesSetup = (props) => {
        // check if props.input.value is populated (shoud be array of ids (numerics))
        // but if it is array of objects, mutate props.input.value to match Numeric[] type
        // and we also need soulution for categorization_date_X & categorization_value_X problem (somehow pass default value for those inputs, perhaps via props.defaults[:categorizationId]['value'|'date'])

        const { input: { value, onChange } } = props;

        const defaultValues = {};
        let selectedValues = [];
        if (value && Array.isArray(value) && value.length) {
            selectedValues = value.map(i => {
                if (!isNaN(i)) {
                    return i;
                }

                if (props.source === 'categorizations') {
                    // populate defautls
                    defaultValues[`_d${i.categorization.id}`] = {};
                    defaultValues[`_d${i.categorization.id}`].value = i.value;
                    defaultValues[`_d${i.categorization.id}`].date = i.year;
                    return String(i.categorization.id);
                }

                return String(get(i, props.optionValue));
            });
        }

        if (selectedValues.length) {
            onChange(selectedValues)
        }
        this.setState({defaultValues, selectedValues});
    }

    handleCheck = (event, isChecked) => {
        const { input: { value, onChange } } = this.props;

        if (isChecked) {
            onChange([...value, ...[event.target.value]]);
        } else {
            // eslint-disable-next-line
            onChange(value.filter(v => v != event.target.value));
        }
    };

    renderCheckbox = choice => {
        const {
            input: { value },
            optionText,
            optionValue,
            options,
            translate,
            translateChoice,
            source
        } = this.props;
        const choiceName = React.isValidElement(optionText) // eslint-disable-line no-nested-ternary
            ? React.cloneElement(optionText, { record: choice })
            : typeof optionText === 'function'
              ? optionText(choice)
              : get(choice, optionText);

        const isChecked = value ? (
            // eslint-disable-next-line
            value.find(v => v == get(choice, optionValue)) !== undefined
          ) : (
            false
          );

        //   console.log('isChecked', value);
        //   console.log('selected', this.state.selectedValues);

        return (
          <div key={get(choice, optionValue)}>
            <Checkbox
                key={get(choice, optionValue)}
                checked={isChecked}
                onCheck={this.handleCheck}
                value={get(choice, optionValue)}
                label={
                    translateChoice ? (
                        translate(choiceName, { _: choiceName })
                    ) : (
                        choiceName
                    )
                }
                {...options}
            />

            { ((checked, choiceID) => {
                if (!checked) return null;

                if ('categorizations' !== source) return null;

                const defaultValues = this.state.defaultValues && this.state.defaultValues[`_d${choiceID}`];

                const theFields = [
                    {
                        name: `categorization_value_${choiceID}`,
                        label: translate('resources.user.fields.categorization_value'),
                        defaultValue: defaultValues ? defaultValues.value : ''
                    },
                    {
                        name: `categorization_date_${choice.id}`,
                        label: translate('resources.user.fields.categorization_date'),
                        defaultValue: defaultValues ? defaultValues.date : ''
                    }
                ];

                return (<div style={styles.subFieldsWrap}>
                    { theFields.map((props, i) => (<span key={i} style={styles.subField}>
                        <Field
                            component={RenderTextInput}
                            validate={required}
                            {...props} />
                    </span>)) }
                </div>)
            })(isChecked, get(choice, optionValue)) }

          </div>
        );
    };

    render() {
        const {
            choices,
            isRequired,
            label,
            muiTheme,
            resource,
            source,
        } = this.props;
        const { selectedValues } = this.state;
        const styles = getStyles(muiTheme);
        const { prepareStyles } = muiTheme;

        return (
            <div>
                <div style={prepareStyles(styles.labelContainer)}>
                    <div style={prepareStyles(styles.label)}>
                        <FieldTitle
                            label={label}
                            source={source}
                            resource={resource}
                            isRequired={isRequired}
                        />
                    </div>
                </div>
                { null !== selectedValues && choices.map(this.renderCheckbox) }
            </div>
        );
    }
}

CustomCheckboxGroupInputComponent.propTypes = {
    addField: PropTypes.bool.isRequired,
    choices: PropTypes.arrayOf(PropTypes.object),
    label: PropTypes.string,
    source: PropTypes.string,
    options: PropTypes.object,
    input: PropTypes.shape({
        onChange: PropTypes.func.isRequired,
    }),
    isRequired: PropTypes.bool,
    optionText: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.func,
        PropTypes.element,
    ]).isRequired,
    optionValue: PropTypes.string.isRequired,
    resource: PropTypes.string,
    translate: PropTypes.func.isRequired,
    translateChoice: PropTypes.bool.isRequired,
    muiTheme: PropTypes.object.isRequired,
};

CustomCheckboxGroupInputComponent.defaultProps = {
    addField: true,
    choices: [],
    options: {},
    optionText: 'name',
    optionValue: 'id',
    translateChoice: true,
};

const enhance = compose(translate, muiThemeable());

const CustomCheckboxGroupInput = enhance(CustomCheckboxGroupInputComponent);

CustomCheckboxGroupInput.propTypes = {
    addField: PropTypes.bool.isRequired,
};

CustomCheckboxGroupInput.defaultProps = {
    addField: true,
};

export default CustomCheckboxGroupInput;
