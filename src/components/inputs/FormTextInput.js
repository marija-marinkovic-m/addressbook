import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MaterialTextField from 'material-ui/TextField';

export class FormTextInput extends Component {
  static propTypes = {
    initialValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  }
  componentDidMount() {
    if (this.props.initialValue) {
      this.props.input.onChange(this.props.initialValue);
    }
  }
  componentWillReceiveProps(nextProps) {
    // if (this.props.initialValue && nextProps.input.value === '') {
    //   this.props.input.onChange(this.props.initialValue);
    // }
  }
  render () {
    const { meta: { touched, error } = {}, input: { ...inputProps }, initialValue,...otherProps } = this.props;
    return (
      <MaterialTextField
        errorText={touched && error}
        {...inputProps}
        {...otherProps} />
    );
  }
}