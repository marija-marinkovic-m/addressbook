import React, { Component } from 'react';
import { Field } from 'redux-form';
import PropTypes from 'prop-types';
import { ReferenceInput, translate } from 'admin-on-rest';

import { AutocompleteInput } from '../inputs/AutocompleteInput';

const noopRenderer = (record) => record.name;

class RendererClass extends Component {
  static propTypes = {
    reference: PropTypes.string,
    filterToQuery: PropTypes.func,
    optionTextRenderer: PropTypes.func,
    searchTextRenderer: PropTypes.func
  }
  static defaultProps = {
    filterToQuery: name => ({ name }),
    optionTextRenderer: noopRenderer,
    searchTextRenderer: noopRenderer
  }
  constructor(props) {
    super(props);
    this.state = {
      autoCompleteOptions: this.getAutocompleteOptions(props)
    }
  }
  getAutocompleteOptions = props => {
    const options = {};
    if (props.input.value === props.record[props.input.name]) {
      options.searchText = props.searchTextRenderer(props.record);
    }
    return options;
  }

  handleUpdateInput = (d) => {
    if (!this.state.autoCompleteOptions) return;
    this.setState({autoCompleteOptions: null})
  }
  render() {
    const { translate, resource, input, optionTextRenderer } = this.props;
    return (
      <ReferenceInput
        label={translate(`resources.${resource}.fields.${input.name}`)}
        {...this.props}>

        <AutocompleteInput
          options={this.state.autoCompleteOptions}
          optionValue="id"
          addField={false}
          optionText={optionTextRenderer}
          onUpdateInput={this.handleUpdateInput}
          translate={translate} />
      </ReferenceInput>
    );
  }
}

const Renderer = translate(RendererClass);


export const EditAutocomplete = ({source, ...other}) => <Field name={source} component={Renderer} {...other} />

