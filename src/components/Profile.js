import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { propTypes, reduxForm, Field } from 'redux-form';
import compose from 'recompose/compose';
import { fetchJson } from 'admin-on-rest/lib/util/fetch';
import { updateProfile as updateProfileAction } from '../actions';
import { API_URL } from '../config';
import { getAuthorizationData } from '../clients';

import { Card, CardTitle } from 'material-ui/Card';
import { Tab, TextField, SelectField, FunctionField, translate } from 'admin-on-rest';

import TabbedShowLayout from '../components/fields/TabbedShowLayout'

import { genderOptions } from '../config';
import { ChipRenderer, CategorizationsChipRenderer } from '../resources/user';

import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import InfoIcon from 'material-ui/svg-icons/action/info';
import { FormTextInput } from './inputs/FormTextInput';
import RaisedButton from 'material-ui/RaisedButton';

// import DatePicker from 'material-ui/DatePicker';
// import { datify, undatify } from './inputs/DateInput';


// const FormDateInput = ({ meta: { touched, error } = {}, input: { value, ...inputProps }, ...otherProps }) => (
//   <DatePicker
//     errorText={touched && error}
//     defaultValue={datify(value)}
//     {...otherProps} />
// );

const EditProfileForm = ({record, onSubmit, translate}) => {
  const fields = [
    'username',
    'address',
    'email',
    'personal_document_number',
    'pak',
    'phone',
    'postal_code',
    'blood_type',

    'profession',
    'work_organization',
    'work_organization_phone',
    'work_organization_address',

    'password',
    'password_confirmation'
  ];

  const hiddenFields = [
    'city_id',
    'club_id',
    'date_joined',
    'gender',
    'pss_member_from',
    'pss_card_number',
    'birthday'
  ];
  return (
    <form onSubmit={onSubmit}>
      { fields.map((fieldName, index) => <Field
        key={fieldName}
        name={fieldName}
        component={FormTextInput}
        floatingLabelText={translate(`resources.user.fields.${fieldName}`)}
        initialValue={record[fieldName]}
        style={styles.formInput} />) }
        
      {/* <Field
        name="birthday"
        component={FormDateInput}
        defaultDate={datify(record.birthday)}
        parse={datify}
        format={undatify}
        formatDate={undatify}
        floatingLabelText={translate('resources.user.fields.birthday')} /> */}

      <div style={{display: 'none'}}>
        { hiddenFields.map(fieldName => <Field
            readOnly
            disabled
            key={fieldName}
            name={fieldName}
            component={FormTextInput}
            initialValue={record[fieldName]} />) }
      </div>

      <RaisedButton primary type="submit" label={translate('pos.submit')} style={styles.formButton} />
    </form>
  );
}

class MyProfileClass extends Component {
  static propTypes = {
    ...propTypes,
    translate: PropTypes.func,
    profileFormSubmitSuccess: PropTypes.bool
  }
  static defaultProps = {
    profileFormSubmitSuccess: false
  }
  state = {
    me: null
  }
  componentDidMount() {
    this.fetchProfile();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.profileFormSubmitSuccess && !this.props.profileFormSubmitSuccess) {
      this.fetchProfile();
    }
  }
  fetchProfile = () => {
    const authorizationData = getAuthorizationData();
    if (false === authorizationData) return;

    fetchJson(`${API_URL}/me`, {user: authorizationData})
      .then(resData => {
        if (resData && resData.json && resData.json.data) {
          this.setState({me: resData.json.data})
        }
      });
  }

  handleUpdateProfile = (data) => {
    this.props.updateProfile(data);
  }

  render() {
    const { translate, handleSubmit } = this.props;
    const { me } = this.state;
    return(
      <Card>
        <CardTitle title={translate('Profile')} />

        { me && <TabbedShowLayout translate={translate} record={me} resource="user">
          <Tab icon={<InfoIcon />} label="info">
            <TextField source="first_name" />
            <TextField source="last_name" />
            <TextField source="email" />
            <TextField source="birthday" />
            <TextField source="personal_document_number" />
            <TextField source="blood_type" />
            <SelectField source="gender" choices={genderOptions} />

            <TextField source="city.name" />
            <TextField source="city.country.name" />
            <TextField source="address" />
            <TextField source="postal_code" />
            <TextField source="pak" />
            <TextField source="phone" />
            <TextField source="phone_2" />

            <TextField source="pss_member_from" />
            <TextField source="club.name" />
            <TextField source="club.city.name" />
            <TextField source="club.city.country.name" />
            <TextField source="date_joined" />
            <TextField source="other_member_legitimacies" />
            <FunctionField source="specialties" render={ChipRenderer} />
            <FunctionField source="titles" render={ChipRenderer} />
            <FunctionField source="categorizations" render={CategorizationsChipRenderer} />
            <FunctionField source="mountaineering_courses" render={ChipRenderer} />


            <TextField source="profession" />
            <TextField source="work_organization" />
            <TextField source="work_organization_phone" />
            <TextField source="work_organization_address" />
          </Tab>

          <Tab icon={<EditIcon />} label="edit">
            <EditProfileForm
              onSubmit={handleSubmit(this.handleUpdateProfile)}
              translate={translate} />
          </Tab>
        </TabbedShowLayout> }

      </Card>
    );
  }
}

const mapStateToProps = (state) => ({
  profileFormSubmitSuccess: state.form['updateProfile-form'].submitSucceeded
});
const mapDispatchToProps = (dispatchEvent) => ({
  updateProfile: bindActionCreators(updateProfileAction, dispatchEvent)
});

const reduxFormObj = {
  form: 'updateProfile-form',
  validate: (values, props) => {
    const errors = {};
    const { translate } = props;

    const requiredFields = [
      'first_name',
      'last_name',
      'username',
      'address',
      'postal_code',
      'pak',
      'phone',
      'email',
      'personal_document_number'
    ];

    const required = translate('aor.validation.required');

    for (let i = 0; i < requiredFields.length; i++) {
      if (!values[requiredFields[i]]) {
        errors[requiredFields[i]] = required;
      }
    }

    return errors;
  }
};

export const MyProfile = compose(
  translate,
  reduxForm(reduxFormObj),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(MyProfileClass);

const styles = {
  formInput: {display: 'block'},
  formButton: {display: 'inline-block', marginTop: '40px'}
}