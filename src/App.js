import React, { Component } from 'react';
import { Resource } from 'admin-on-rest';
import Admin from './Admin';

import { authClient, restClient } from './clients';
import { Dashboard, LoginPage, CustomMenu, Layout } from './components';
import translations from './i18n';

import { API_URL, nestedAdministrationLinks, mainAppResources } from './config';

import './App.css';
import { storage } from './util';

import customSagas from './sagas';
import customRoutes from './routes';
import customReducers from './reducers';


const sLocale = storage.getItem('lang') || 'en';

class App extends Component {
  render() {
    return (
      <Admin
        menu={CustomMenu}
        appLayout={Layout}
        title="pos.adminTitle"
        authClient={authClient.bind(null, API_URL)}
        restClient={restClient(API_URL)}
        locale={sLocale}
        messages={translations}
        dashboard={Dashboard}
        loginPage={LoginPage}
        customReducers={customReducers}
        customSagas={customSagas}
        customRoutes={customRoutes}>

        { mainAppResources
            .map(resParams => <Resource {...resParams} />) }
        
        { nestedAdministrationLinks
            .map(item => <Resource key={item.key} name={item.key} {...item.resParams} />) }

      </Admin>
    );
  }
}

export default App;
