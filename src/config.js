import {
  CountryList, CountryCreate, CountryEdit,
  CityList, CityEdit, CityCreate,
  CourseList, CourseEdit, CourseCreate,
  SpecialtyList, SpecialtyCreate, SpecialtyEdit,
  TitleList, TitleCreate, TitleEdit,
  CategorizationList, CategorizationCreate, CategorizationEdit, CategorizationShow, CityShow, CountryShow, CourseShow, SpecialtyShow, TitleShow,
  UserUnresolvedList, UserUnresolvedShow, UserShow, UserList, UserEdit, UserCreate,
  ClubShow, ClubList, ClubEdit, ClubCreate, MembershipFeeList,
  DocumentTypeList, DocumentTypeCreate, DocumentTypeEdit, DocumentTypeShow
} from './resources';

import { Delete } from 'admin-on-rest';
import { storage } from './util';

export const API_URL = process.env.REACT_APP_API_URL || 'http://pss-adresar.etl.yt';

export const NEWS_URL = 'https://pss.rs/wp-json/wp/v2/posts?per_page=1';
export const EVENTS_URL = 'https://pss.rs/wp-json/tribe/events/v1/events';

export const apiDateFormat = 'DD.MM.YYYY';

export const rightsOptions = [
  { id: 'user', name: 'Regular' },
  { id: 'admin', name: 'Admin' },
  { id: 'head_admin', name: 'Super Admin' }
];
export const genderOptions = [
  { id: 'male', name: 'Male' },
  { id: 'female', name: 'Female' }
];

// @TBD: use storage.getItem('rights') to determine if list/edit/delete/create should exist inside individual resParams prop

export const mainAppResources = ((storage) => {
  const role = parseInt(storage.getItem('rights'), 10);

  return [
    {
      key: 'user/unresolved',
      name: 'user/unresolved',
      list: role > 1 ? UserUnresolvedList : null,
      show: role > 1 ? UserUnresolvedShow : null
    },
    {
      key: 'user',
      name: 'user',
      show: UserShow,
      list: UserList,
      edit: role > 0 ? UserEdit : null,
      create: role > 0 ? UserCreate : null,
      remove: role > 0 ? Delete : null
    },
    {
      key: 'club',
      name: 'club',
      show: ClubShow,
      list: role > 0 ? ClubList : null,
      edit: role > 0 ? ClubEdit : null,
      create: role > 1 ? ClubCreate : null,
      remove: role > 1 ? Delete : null
    },
    {
      key: 'membership-fee',
      name: 'membership-fee',
      list: role > 0 ? MembershipFeeList : null
    }
  ];
})(storage);

export const nestedAdministrationLinks = [
  {
    primaryText: 'resources.country.name',
    linkTo: '/country',
    key: 'country',
    resParams: {
      list: CountryList,
      show: CountryShow,
      create: CountryCreate,
      edit: CountryEdit,
      remove: Delete
    }
  },
  {
    primaryText: 'resources.city.name',
    linkTo: '/city',
    key: 'city',
    resParams: {
      list: CityList,
      show: CityShow,
      edit: CityEdit,
      create: CityCreate,
      remove: Delete
    }
  },
  {
    primaryText: 'resources.mountaineering-course.name',
    linkTo: '/mountaineering-course',
    key: 'mountaineering-course',
    resParams: {
      list: CourseList,
      show: CourseShow,
      edit: CourseEdit,
      create: CourseCreate,
      remove: Delete
    }
  },
  {
    primaryText: 'resources.specialty.name',
    linkTo: '/specialty',
    key: 'specialty',
    resParams: {
      list: SpecialtyList,
      show: SpecialtyShow,
      edit: SpecialtyEdit,
      create: SpecialtyCreate,
      remove: Delete
    }
  },
  {
    primaryText: 'resources.title.name',
    linkTo: '/title',
    key: 'title',
    resParams: {
      list: TitleList,
      show: TitleShow,
      edit: TitleEdit,
      create: TitleCreate,
      remove: Delete
    }
  },
  {
    primaryText: 'resources.categorization.name',
    linkTo: '/categorization',
    key: 'categorization',
    resParams: {
      list: CategorizationList,
      show: CategorizationShow,
      edit: CategorizationEdit,
      create: CategorizationCreate,
      remove: Delete
    }
  },
  {
    primaryText: 'resources.personal-document-type.name',
    linkTo: '/personal-document-type',
    key: 'personal-document-type',
    resParams: {
      list: DocumentTypeList,
      show: DocumentTypeShow,
      edit: DocumentTypeEdit,
      create: DocumentTypeCreate,
      remove: Delete
    }
  }
];