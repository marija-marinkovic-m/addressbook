export const START_FETCH_EVENTS = '@WPAPI/EVENTS_FETCH_START';
export const END_FETCH_EVENTS = '@WPAPI/EVENTS_FETCH_END';

export const fetchEvents = ({url, cb}) => ({
  type: START_FETCH_EVENTS,
  payload: {url, cb}
});
export const eventsFetched = (events) => ({
  type: END_FETCH_EVENTS,
  payload: events
});