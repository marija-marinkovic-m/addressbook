import { DELETE } from 'admin-on-rest';

export const GET_ONE_RESOLVE = 'GET_ONE_RESOLVE';
export const USER_APPROVE = 'USER_APPROVE';
export const USER_REJECT = 'USER_REJECT';

export const USER_APPROVE_SUCCESS = 'USER_APPROVE_SUCCESS';
export const USER_APPROVE_FAILURE = 'USER_APPROVE_FAILURE';

export const USER_REJECT_SUCCESS = 'USER_REJECT_SUCCESS';
export const USER_REJECT_FAILURE = 'USER_REJECT_FAILURE';

export const userApprove = (id, data, basePath) => ({
  type: USER_APPROVE,
  payload: { id },
  meta: { resource: 'user', fetch: GET_ONE_RESOLVE, cancelPrevious: false }
});

export const userReject = (id, data, basePath) => ({
  type: USER_REJECT,
  payload: { id },
  meta: { resource: 'user', fetch: DELETE, cancelPrevious: false }
});