import { DELETE } from 'admin-on-rest';

export const DELETE_MEMBERSHIP_FEE = 'DELETE_MEMBERSHIP_FEE';
export const DELETE_MEMBERSHIP_FEE_SUCCESS = 'DELETE_MEMBERSHIP_FEE_SUCCESS';
export const DELETE_MEMBERSHIP_FEE_FAILURE = 'DELETE_MEMBERSHIP_FEE_FAILURE';

export const deleteMembershipFee = (id, basePath) => ({
  type: DELETE_MEMBERSHIP_FEE,
  payload: { id, basePath },
  meta: { resource: 'membership-fee', fetch: DELETE, cancelPrevious: false }
});