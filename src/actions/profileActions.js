export const UPDATE_PROFILE = '@pss/UPDATE_PROFILE';

export const UPDATE_PROFILE_SUCCESS = '@pss/UPDATE_PROFILE_SUCCESS';
export const UPDATE_PROFILE_FAILURE = '@pss/UPDATE_PROFILE_FAILURE';

export const updateProfile = (data) => ({
  type: UPDATE_PROFILE,
  payload: { data },
  meta: { resource: 'me', fetch: UPDATE_PROFILE, cancelPrevious: false }
});