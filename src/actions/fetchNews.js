export const START_FETCH_NEWS = '@WPAPI/NEWS_FETCH_START';
export const END_FETCH_NEWS = '@WPAPI/NEWS_FETCH_END';

export const UPDATE_NEWS_FEATURED_IMG = '@WPAPI/UPDATE_NEWS_FEATURED_IMG';

export const fetchNews = ({url, cb}) => ({
  type: START_FETCH_NEWS,
  payload: {url, cb}
});
export const newsFetched = (news) => ({
  type: END_FETCH_NEWS,
  payload: news
});
export const updateNewsFeaturedImg = (postId, featuredSrc) => ({
  type: UPDATE_NEWS_FEATURED_IMG,
  payload: {postId, featuredSrc}
});