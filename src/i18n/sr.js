export default {
  pos: {
    search: 'Pretraga',
    configuration: 'Konfiguracija',
    language: 'Jezik',
    theme: {
      name: 'Tema',
      light: 'Svetla',
      dark: 'Tamna'
    },
    adminTitle: 'PSS Adresar',
    brandName: 'Planinarski Savez Srbije',
    submit: 'Sačuvaj'
  },
  resources: {
    user: {
      name: 'Karton |||| Članovi',
      fields: {
        first_name: 'Ime',
        last_name: 'Prezime',
        email: 'Email',
        birthday: 'Datum rođenja',
        username: 'Korisničko ime',
        gender: 'Pol',
        city_id: 'Mesto boravišta',
        city: {
          name: 'Mesto boravišta',
          country: {name: 'Država boravišta'}
        },
        birth_city_id: 'Mesto rođenja',
        birth_city: {
          name: 'Mesto rođenja',
          country: { name: 'Država rođenja' }
        },
        address: 'Adresa boravišta',
        postal_code: 'Poštanski broj',
        pak: 'PAK',
        phone: 'Telefon',
        mobile_phone: 'Mobilni telefon',
        blood_type: 'Krvna grupa',
        profession: 'Zanimanje',
        work_organization: 'Zaposlenje',
        work_organization_phone: 'Poslovni telefon',
        work_organization_address: 'Poslovna adresa',
        pss_member_from: 'Član PSS-a od',
        pss_card_number: 'Broj članske karte (PSS)',
        personal_document_type_id: 'Tip dokumenta',
        personal_document_number: 'Broj ličnog dokumenta',
        rights: 'Uloga',
        other_member_legitimacies: 'Legitimacije ostalih članova',
        active: 'Aktivan',
        club_id: 'Planinarski klub',
        club: {
          name: 'Klub',
          city: {
            name: 'Mesto kluba',
            country: {name: 'Država kluba'}
          }
        },
        title_id: 'Zvanje',
        specialty_id: 'Interesovanja',
        mountaineering_course_id: 'Završeni planinarski tečaj',
        titles: 'Stečena zvanja',
        specialties: 'Specijalnosti za koje je zainteresovan',
        categorizations: 'Kategorizacije',
        categorization_value: 'Specijalnost kategorizacije',
        categorization_date: 'Godine',
        mountaineering_courses: 'Završeni planinarski tečajevi',
        password: 'Lozinka',
        password_confirmation: 'Potvrdi lozinku',
        date_joined: 'Datum pristupanja',
        created_by: 'Karton napravio korisnik: ',
        resolve: 'Aktiviraj korisnika',
        remove: 'Obriši karton',
        createMembership: {
          label: 'Članstvo',
          info: 'Markice se mogu dodati tek nakon što je unos sačuvan u bazi.'
        }
      },
      tabs: {
        basic: 'Osnovno',
        location: 'Lokacija',
        additional: 'Dodatno',
        pss: 'PSS',
        membership: 'Markice (članstvo)'
      },
      user_name: 'Ime'
    },
    'user/unresolved': {
      name: 'Nerešeni konflikt |||| Nerešeni konflikti'
    },
    club: {
      name: 'Klub |||| Klubovi',
      fields: {
        name: 'Naziv',
        city_id: 'Mesto',
        city: 'Mesto',
        country: {
          name: 'Država'
        },
        contact_person: {
          name: 'Osoba za kontakt'
        },
        contact_person_id: 'Osoba za kontakt',
        address: 'Adresa',
        phone_number: 'Broj telefona kontakt osobe',
        year_of_establishment: 'Godina osnivanja',
        description: 'Opis',
        monthly_membership_fee: 'Mesečna članarina',
        active: 'Aktivan'
      },
      tabs: {
        basic: 'O klubu',
        membership: 'Članstvo (markice)',
        admins: 'Administratori'
      },
      assign_admin: 'Dodeli administratora klubu'
    },
    country: {
      name: 'Država |||| Države',
      fields: {
        name: 'Naziv',
        country_iso_code: 'ISO',
        clubId: 'Klub'
      }
    },
    city: {
      name: 'Mesto (grad, selo) |||| Mesta',
      fields: {
        name: 'Naziv mesta (grad, selo)',
        country_id: 'Država',
        country: {
          name: 'Država',
          country_iso_code: 'ISO'
        }
      }
    },
    'mountaineering-course': {
      name: 'Tečaj |||| Tečajevi',
      fields: {
        name: 'Naziv',
        description: 'Opis'
      }
    },
    specialty: {
      name: 'Interesovanje |||| Interesovanja',
      fields: {
        name: 'Naziv',
        description: 'Opis'
      }
    },
    title: {
      name: 'Zvanje |||| Zvanja',
      fields: {
        name: 'Naziv',
        description: 'Opis'
      }
    },
    categorization: {
      name: 'Kategorizacija |||| Kategorizacije',
      fields: {
        name: 'Naziv',
        description: 'Opis',
        field_name: 'Naziv polja'
      }
    },
    'membership-fee': {
      name: 'Članarina |||| Članarine',
      all: 'Sve markice ovog člana',
      nameAll: 'Sve markice ovog kluba',
      fields: {
        club_id: 'Klub',
        user_id: 'Član',
        membership_fee_for_year: 'Godina',
        comment: 'Komentar',
        stamp_number: 'Broj markice',
        amount: 'Iznos',
        year_from: 'Od godine',
        year_to: 'Do godine'
      },
      create: 'Napravi markicu za %{first_name} %{last_name}',
      new: 'Nova markica'
    },
    'personal-document-type': {
      name: 'Tip dokumenta |||| Tipovi dokumentata',
      fields: {
        name: 'Naziv',
        description: 'Opis'
      }
    }
  },
  misc: {
    otherResources: 'Osnovni resursi',
    administration: 'Administracija',
    settings: 'Podešavanja',
    myProfile: 'Nalog',
    news: 'Sve vesti',
    readMore: 'Pročitaj više...',
    published: 'Objavljeno',
    calendar: 'Kalendar Aktivnosti',
    allEvents: 'Sve aktivnosti',
    users: 'Članova',
    clubs: 'Klubova',
    unresolvedConflicts: 'Nerešenih konflikata',
    searchUsers: {
      title: 'Pretraga članova',
      subtitle: 'Unesite kriterijume po kojima želite da pretražujete članove',
      cardSubtitle: 'Pretražujte članove po imenu, prezimenu, završenim tečajevima, stečenim zvanjima itd.',
      fields: {
        club: 'Klub',
        name: 'Deo imena ili prezimena',
        personal_document_number: 'Lični broj',
        pss_card_number: 'Broj članske karte',
        mountaineering_course_id: 'Završeni planinarski tečajevi',
        specialty_id: 'Specijalnosti za koje je zainteresovan',
        title_id: 'Stečena zvanja',
        orderBy: 'Sortiranje',
        cancel: 'Otkaži',
        submit: 'Pokreni pretragu...'
      }
    },
    submitFailed: 'Nije uspelo. Unos nije kompletan / validan. Molimo, proverite polja.',
    conflictResolved: 'Konflikt uspešno rešen.',
    conflictResolveFailed: 'Konflikt nije rešen, desila se greška. Pokušajte ponovo kasnije.',
    profile_updated: 'Izmene naloga sačuvane.',
    profile_update_failed: 'Greška. Izmene nisu sačuvane. Proverite polja.'
  },
  Unauthorized: 'Neautorizovan',
  Male: 'Muški',
  Female: 'Ženski',
  City: 'Grad',
  'Unprocessable Entity': 'Nije uspelo, proverite formu zahteva.',
  edit: 'Uredi',
  Profile: 'Nalog',
  Club: 'Klub'
}