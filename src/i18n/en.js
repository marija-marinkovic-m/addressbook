export default {
  pos: {
    search: 'Search',
    configuration: 'Configuration',
    language: 'Language',
    theme: {
      name: 'Theme',
      light: 'Light',
      dark: 'Dark'
    },
    adminTitle: 'PSS Address book',
    brandName: 'Mountaineering Association of Serbia',
    submit: 'Submit'
  },
  resources: {
    user: {
      name: 'User |||| Users',
      fields: {
        first_name: 'First Name',
        last_name: 'Last Name',
        city_id: 'City',
        email: 'Email',
        city: {
          name: 'City',
          country: { name: 'Country' }
        },
        birth_city_id: 'Birthplace',
        birth_city: {
          name: 'Birthplace',
          country: { name: 'Birth Country' }
        },
        club_id: 'Club',
        mobile_phone: 'Mobile phone number',
        title_id: 'Title',
        specialty_id: 'Specialty',
        club: {
          name: 'Club',
          city: {
            name: 'Club city',
            country: { name: 'Club country' }
          }
        },
        personal_document_type_id: 'Document type',
        personal_document_number: 'Document ID',
        blood_type: 'Blood type',
        profession: 'Profession',
        work_organization: 'Work Organization',
        work_organization_phone: 'Work Phone',
        work_organization_address: 'Work Address',
        username: 'Username',
        address: 'Adress',
        pak: 'PAK',
        phone: 'Phone',
        postal_code: 'Postal Code',
        password: 'New Password',
        password_confirmation: 'Confirm New Password',
        createMembership: {
          label: 'Membership',
          info: 'Membership info cannt be attached before the entry is successfully stored into the database.'
        }
      },
      tabs: {
        basic: 'Basic',
        location: 'Location',
        additional: 'Additional',
        pss: 'PSS',
        membership: 'Membership'
      },
      user_name: 'Name'
    },
    'user/unresolved': {
      name: 'Unresolved conflict |||| Unresolved conflicts'
    },
    club: {
      name: 'Club |||| Clubs',
      city_id: 'City',
      city: {
        name: 'City'
      },
      country: {
        name: 'Country'
      },
      contact_person: {
        name: 'Contact Person'
      },
      tabs: {
        basic: 'Basic Information',
        membership: 'Membership',
        admins: 'Administrators'
      },
      assign_admin: 'Assign Administrator to the club',
      fields: {
        phone_number: "Contact person's phone number",
        contact_person_id: "Contact person"
      }
    },
    country: {
      name: 'Country |||| Countries',
      fields: {
        clubId: 'Klub'
      }
    },
    city: {
      name: 'City |||| Cities',
      fields: {
        country_id: 'Country',
        country: {
          name: 'Country',
          country_iso_code: 'ISO Code'
        }
      }
    },
    'mountaineering-course': {
      name: 'Course |||| Courses'
    },
    specialty: {
      name: 'Specialty |||| Specialities'
    },
    title: {
      name: 'Vocation |||| Vocations'
    },
    categorization: {
      name: 'Categorization |||| Categorizations'
    },
    'membership-fee': {
      name: 'Membership fee |||| Membership fees',
      all: 'All Memberships',
      create: 'Create membership fee for %{first_name} %{last_name}',
      new: 'Create Membership',
      nameAll: 'All Club Memberships',
      fields: {
        membership_fee_for_year: 'Membership fee for year',
        stamp_number: 'Stamp number',
        user_id: 'User',
        club_id: 'Club'
      }
    },
    'personal-document-type': {
      name: 'Document type |||| Document types'
    }
  },
  misc: {
    otherResources: 'Basic Resources',
    administration: 'Administration',
    settings: 'Settings',
    myProfile: 'Profile',
    news: 'All News',
    readMore: 'Read More...',
    published: 'Published',
    calendar: 'Events',
    allEvents: 'All Events',
    users: 'Members',
    clubs: 'Clubs',
    unresolvedConflicts: 'Unresolved conflicts',
    searchUsers: {
      title: 'Search Users',
      subtitle: 'Please, enter search criteria',
      cardSubtitle: 'Search users by first name, last name, finished courses, specialities, titles,...',
      fields: {
        club: 'Club',
        name: 'First name / Last name parts',
        personal_document_number: 'Document ID',
        pss_card_number: 'Card number',
        mountaineering_course_id: 'Mountaineering Courses',
        specialty_id: 'Interests',
        title_id: 'Titles',
        orderBy: 'Order By',
        cancel: 'Cancel',
        submit: 'Submit'
      }
    },
    submitFailed: 'Submit Failed. Not all fields are valid. Please, check your entry.',
    conflictResolved: 'Conflict successfully resolved.',
    conflictResolveFailed: 'Error happened. Please, try later.',
    profile_updated: 'Profile Updated Successfully',
    profile_update_failed: 'Profile Update Failed. Please, try later.'
  },
  Unauthorized: 'Unauthorized',
  City: 'City',
  Profile: 'Profile'
}