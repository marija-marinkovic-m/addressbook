export const enDefault = {
  aor: {
      action: {
          delete: 'Delete',
          show: 'Show',
          list: 'List',
          save: 'Save',
          create: 'Create',
          edit: 'Edit',
          cancel: 'Cancel',
          refresh: 'Refresh',
          add_filter: 'Add filter',
          remove_filter: 'Remove this filter',
          back: 'Go Back',
      },
      boolean: {
          true: 'Yes',
          false: 'No',
      },
      page: {
          list: '%{name} List',
          edit: '%{name} #%{id}',
          show: '%{name} #%{id}',
          create: 'Create %{name}',
          delete: 'Delete %{name} #%{id}',
          dashboard: 'Dashboard',
          not_found: 'Not Found',
      },
      input: {
          file: {
              upload_several:
                  'Drop some files to upload, or click to select one.',
              upload_single: 'Drop a file to upload, or click to select it.',
          },
          image: {
              upload_several:
                  'Drop some pictures to upload, or click to select one.',
              upload_single:
                  'Drop a picture to upload, or click to select it.',
          },
      },
      message: {
          yes: 'Yes',
          no: 'No',
          are_you_sure: 'Are you sure?',
          about: 'About',
          not_found:
              'Either you typed a wrong URL, or you followed a bad link.',
      },
      navigation: {
          no_results: 'No results found',
          page_out_of_boundaries: 'Page number %{page} out of boundaries',
          page_out_from_end: 'Cannot go after last page',
          page_out_from_begin: 'Cannot go before page 1',
          page_range_info: '%{offsetBegin}-%{offsetEnd} of %{total}',
          next: 'Next',
          prev: 'Prev',
      },
      auth: {
          username: 'Username',
          email: 'Email',
          password: 'Password',
          sign_in: 'Sign in',
          sign_in_error: 'Authentication failed, please retry',
          user_not_found: 'User not found',
          missing_token: 'Authentication failed, server error (missing_token)',
          logout: 'Logout',
          unauthorized: 'Unauthorized access'
      },
      notification: {
          updated: 'Element updated',
          created: 'Element created',
          deleted: 'Element deleted',
          item_doesnt_exist: 'Element does not exist',
          http_error: 'Server communication error',
          admin_assigned: 'Administrator assigned to the club'
      },
      validation: {
          required: 'Required',
          minLength: 'Must be %{min} characters at least',
          maxLength: 'Must be %{max} characters or less',
          minValue: 'Must be at least %{min}',
          maxValue: 'Must be %{max} or less',
          number: 'Must be a number',
          email: 'Must be a valid email',
      },
  },
};

export const srDefault = {
  aor: {
      action: {
          delete: 'Obriši',
          show: 'Prikaži',
          list: 'Lista',
          save: 'Sačuvaj',
          create: 'Napravi',
          edit: 'Uredi',
          cancel: 'Otkaži',
          refresh: 'Osveži',
          add_filter: 'Dodaj filter',
          remove_filter: 'Ukloni filter',
          back: 'Nazad',
      },
      boolean: {
          true: 'Da',
          false: 'Ne',
      },
      page: {
          list: '%{name}',
          edit: '%{name} #%{id}',
          show: '%{name} #%{id}',
          create: 'Napravi %{name}',
          delete: 'Obriši %{name} #%{id}',
          dashboard: 'Kontrolna tabla',
          not_found: 'Nije pronađeno',
      },
      input: {
          file: {
              upload_several:
                  'Prevuci nekoliko datoteka ili klikni za odabir pojedinačne.',
              upload_single: 'Prevuci datoteku za slanje ili klikni za odabir.',
          },
          image: {
            upload_several:
            'Prevuci nekoliko datoteka ili klikni za odabir pojedinačne.',
            upload_single: 'Prevuci datoteku za slanje ili klikni za odabir.',
          },
      },
      message: {
          yes: 'Da',
          no: 'Ne',
          are_you_sure: 'Da li ste sigurni?',
          about: 'O nama',
          not_found:
              'Ili ste ukucali pogresan URL, ili kliknuli netačan link.',
      },
      navigation: {
          no_results: 'Nema pronađenih rezultata',
          page_out_of_boundaries: 'Nepostojeća strana: %{page}',
          page_out_from_end: 'Ne možemo preskočiti poslednju stranu.',
          page_out_from_begin: 'Nema strana pre prve strane.',
          page_range_info: '%{offsetBegin}-%{offsetEnd} od %{total}',
          next: 'Sledeća',
          prev: 'Predhodna',
      },
      auth: {
          username: 'Korisničko ime',
          email: 'Email',
          password: 'Lozinka',
          sign_in: 'Prijava',
          sign_in_error: 'Autentifikacija nije uspela, molimo pokušajte ponovo',
          user_not_found: 'Korisnik nije pronađen, molimo proverite lozinku i email adresu',
          missing_token: 'Autentifikacija nije uspela, serverska greska (missing_token)',
          logout: 'Odjava',
          unauthorized: 'Nije dozvoljeno'
      },
      notification: {
          updated: 'Unos sačuvan',
          created: 'Unos kreiran',
          deleted: 'Unos izbrisan',
          item_doesnt_exist: 'Unos ne postoji',
          http_error: 'Greška u komunikaciji sa serverom',
          admin_assigned: 'Administrator dodeljen klubu'
      },
      validation: {
          required: 'Obavezno polje',
          minLength: 'Minimalan dozvoljen broj karaktera: %{min}',
          maxLength: 'Dozvoljen maksimalan %{max} broj karaktera',
          minValue: 'Minimalna vrednost: %{min}',
          maxValue: 'Mora biti %{max} ili manje',
          number: 'Mora biti broj',
          email: 'Uneli ste nevažeću email adresu.',
      },
  },
};