import { enDefault, srDefault } from './default';

import enCustom from './en';
import srCustom from './sr';

export default {
  sr: {...srDefault, ...srCustom},
  en: {...enDefault, ...enCustom}
}