import { all } from 'redux-saga/effects';
import auth from 'admin-on-rest/lib/sideEffect/saga/auth';
import crudFetch from 'admin-on-rest/lib/sideEffect/saga/crudFetch';
import crudResponse from './crudResponse';
import referenceFetch from 'admin-on-rest/lib/sideEffect/saga/referenceFetch';

/**
 * @param {Object} restClient A REST object with two methods: fetch() and convertResponse()
 */
export default (restClient, authClient) =>
  function* crudSaga() {
    yield all([
      auth(authClient)(),
      crudFetch(restClient)(),
      crudResponse(),
      referenceFetch(),
    ]);
  };