import { put, takeEvery, all } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { showNotification } from 'admin-on-rest';
import { USER_APPROVE_SUCCESS, USER_REJECT_SUCCESS, USER_APPROVE_FAILURE, USER_REJECT_FAILURE } from '../actions';

function* conflictAdminSuccess() {
  yield put(showNotification('misc.conflictResolved'));
  yield put(push('/')); // to refresh if on UnrelsolvedList
  yield put(push('/user/unresolved'));
}

function* conflictAdminFailure({error}) {
  yield put(showNotification('misc.conflictResolveFailed', 'warning'));
  console.error(error);
}

export default function* conflictSaga() {
  yield all([
    takeEvery(USER_APPROVE_SUCCESS, conflictAdminSuccess),
    takeEvery(USER_REJECT_SUCCESS, conflictAdminSuccess),
    takeEvery(USER_APPROVE_FAILURE, conflictAdminFailure),
    takeEvery(USER_REJECT_FAILURE, conflictAdminFailure)
  ]);
}

