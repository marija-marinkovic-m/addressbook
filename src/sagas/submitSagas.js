import { put, takeEvery, all } from 'redux-saga/effects';
import { showNotification, CRUD_CREATE_FAILURE } from 'admin-on-rest';
import { stopSubmit } from 'redux-form';

import { SET_SUBMIT_FAILED } from 'redux-form/lib/actionTypes';

export const extractViolations = data => {
  const res = {};
  for (const prop in data) {
    if (data[prop].length && data[prop][0]) {
      res[prop] = data[prop][0]
    }
  }

  return res;
}

function* submitFailed() {
  yield put(showNotification('misc.submitFailed'));
}

function* crudCreateFailure(action) {
  var json = action.payload;
  const violations = extractViolations(json.data);
  yield put(stopSubmit('record-form', violations))

  // rollbar log
  if (window.Rollbar) window.Rollbar.warning(`Form (page: ${window.location.href}) submition violations: ${JSON.stringify(json.data)}`);
}

export default function* customSubmitSaga() {
  yield all([
    takeEvery(SET_SUBMIT_FAILED, submitFailed),
    takeEvery(CRUD_CREATE_FAILURE, crudCreateFailure)
  ]);
}