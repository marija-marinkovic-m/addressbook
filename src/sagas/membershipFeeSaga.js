import { put, takeEvery, all } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { showNotification } from 'admin-on-rest';

import { DELETE_MEMBERSHIP_FEE_SUCCESS, DELETE_MEMBERSHIP_FEE_FAILURE } from '../actions';

function* deleteMembershipFeeSuccess(data) {
  console.log(data);
  yield put(showNotification('aor.notification.deleted'));

  if (data.requestPayload && data.requestPayload.basePath) {
    yield put(push('/'));
    yield put(push(data.requestPayload.basePath));
  }
}
function* deleteMembershipFeeFailure({error}) {
  yield put(showNotification('aor.notification.http_error', 'warning'));
}

export default function* membershipFeeSaga() {
  yield all([
    takeEvery(DELETE_MEMBERSHIP_FEE_SUCCESS, deleteMembershipFeeSuccess),
    takeEvery(DELETE_MEMBERSHIP_FEE_FAILURE, deleteMembershipFeeFailure)
  ])
}
