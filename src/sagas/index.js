import customSubmitSaga from './submitSagas';
import conflictSaga from './conflictSaga';
import membershipFeeSaga from './membershipFeeSaga';
import profileSaga from './profileSaga';

export default [
  customSubmitSaga,
  conflictSaga,
  membershipFeeSaga,
  profileSaga
];