import { put, takeEvery, all } from 'redux-saga/effects';
import { showNotification } from 'admin-on-rest';

import { UPDATE_PROFILE_SUCCESS, UPDATE_PROFILE_FAILURE } from '../actions';
import { extractViolations } from './submitSagas';
import { stopSubmit } from 'redux-form';

function* updateProfileSuccess() {
  yield put(showNotification('misc.profile_updated'));
}

function* updateProfileFailure(action) {
  var json = action.payload;
  const violations = extractViolations(json.data);
  yield put(stopSubmit('updateProfile-form', violations));
  yield put(showNotification('misc.profile_update_failed', 'warning'));

  // rollbar log
  if (window.Rollbar) window.Rollbar.warning(`Profile form submition violations: ${JSON.stringify(json.data)}`);
}

export default function* profileSaga() {
  yield all([
    takeEvery(UPDATE_PROFILE_SUCCESS, updateProfileSuccess),
    takeEvery(UPDATE_PROFILE_FAILURE, updateProfileFailure)
  ]);
}