import React from 'react';
import { List, Datagrid, TextField, EditButton, Edit, SimpleForm, LongTextInput, DisabledInput, TextInput, required, Create, Show, SimpleShowLayout, RichTextField, Filter } from 'admin-on-rest';
import { ExcerptField } from '../../util/index';
import { ShowLinkField } from '../../components/buttons/ShowResourceLink';

const DocumentTypeFilters = props => (
  <Filter {...props}>
    <TextInput label="pos.search" source="name" alwaysOn />
  </Filter>
);

export const DocumentTypeList = (props) => (
  <List {...props} filters={<DocumentTypeFilters />}>
    <Datagrid>
      <TextField source="id" />
      <ShowLinkField />
      <ExcerptField source="description" maxLength={80} />
      <EditButton />
    </Datagrid>
  </List>
);

export const DocumentTypeCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" validate={required} />
      <LongTextInput source="description" />
    </SimpleForm>
  </Create>
);

export const DocumentTypeEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="name" validate={required} />
      <LongTextInput source="description" />
    </SimpleForm>
  </Edit>
);

export const DocumentTypeShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="name" />
      <RichTextField source="description" />
    </SimpleShowLayout>
  </Show>
);