import React from 'react';

import { List, Datagrid, TextField, Create, SimpleForm, TextInput, required, Edit, DisabledInput, EditButton, Show, SimpleShowLayout } from 'admin-on-rest';
import { ShowLinkField } from '../../components/buttons/ShowResourceLink';

export const CountryList = (props) => (
  <List {...props}>
    <Datagrid>
      <TextField source="id" />
      <ShowLinkField />
      <TextField source="country_iso_code" />
      <EditButton />
    </Datagrid>
  </List>
);


export const CountryCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" validate={required} />
      <TextInput source="country_iso_code" validate={required} />
    </SimpleForm>
  </Create>
);

export const CountryEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="name" validate={required} />
      <TextInput source="country_iso_code" validate={required} />
    </SimpleForm>
  </Edit>
);

export const CountryShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="name" />
      <TextField source="country_iso_code" />      
    </SimpleShowLayout>
  </Show>
);