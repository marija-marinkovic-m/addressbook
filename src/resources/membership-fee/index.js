import React from 'react';
import { List, Datagrid, TextField, ReferenceInput, NumberInput, Filter, ReferenceField, FunctionField } from 'admin-on-rest';
import { fullNameRenderer, storage } from '../../util';

import AutocompleteInput from '../../components/inputs/AutocompleteInput';
import { DeleteMembershipFeeButton } from '../../components/buttons/DeleteMembershipFeeButton';

const Filters = (props) => {
  const currUserClubID = storage.getItem('club_id');
  if (!currUserClubID || storage.getItem('rights') < 2) return null;

  const currentYear = (new Date()).getFullYear();
  const validateYear = value => {
    if (value < 1901 || value > currentYear) {
      return true;
    }
    return false;
  }

  return (
    <Filter {...props}>
      <ReferenceInput
        source="club_id"
        reference="club"
        filterToQuery={name => ({name})}
        alwaysOn={true}>
        <AutocompleteInput
          optionText="name"
          initialValue={currUserClubID} />
      </ReferenceInput>
      <ReferenceInput
        source="user_id"
        reference="user"
        filterToQuery={name => ({name})}
        allowEmpty alwaysOn={false}>
        <AutocompleteInput optionText={fullNameRenderer} />
      </ReferenceInput>
      <NumberInput
        source="year_from"
        step={1}
        options={{min: '1901', max: currentYear-1}}
        validate={validateYear} />
      <NumberInput
        source="year_to"
        step={1}
        options={{min: '1902', max: currentYear}}
        validate={validateYear} />
    </Filter>
  );
};

export const MembershipFeeList = (props) => (
  <List {...props} filters={<Filters />}>
    <Datagrid>
      <TextField source="id" />
      <ReferenceField source="user_id" reference="user">
        <FunctionField render={(r,s) => r && fullNameRenderer(r)} />
      </ReferenceField>
      <TextField source="membership_fee_for_year" />
      {/* <TextField source="amount" /> */}
      <TextField source="stamp_number" />
      <DeleteMembershipFeeButton />
    </Datagrid>
  </List>
);