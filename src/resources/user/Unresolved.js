import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { List, Datagrid, TextField, SimpleShowLayout, EmailField, SelectField, FunctionField, translate, ReferenceField } from 'admin-on-rest';
import { fullNameRenderer, clubRenderer } from '../../util';
import { genderOptions } from '../../config';
import { ShowLinkField } from '../../components/buttons/ShowResourceLink';
import { connect } from 'react-redux';

import { Card, CardActions, CardTitle } from 'material-ui/Card';
import { ChipRenderer } from '../user';

import { ConflictButton } from '../../components/buttons/ConflictButton';

export const UserUnresolvedList = (props) => (
  <List {...props}>
    <Datagrid>
      <TextField source="id" />
      <ShowLinkField label="resources.user.user_name" source="first_name" />
      <FunctionField sortable={false} label="resources.user.fields.club_id" source="club" render={r => r && clubRenderer(r.club)} />

      <ConflictButton label="resources.user.fields.resolve" />
      <ConflictButton label="resources.user.fields.remove" approve={false} />
    </Datagrid>
  </List>
);

class UserUnresolvedShowClass extends Component {

  render () {
    const data = this.props.unresolved && this.props.unresolved.data && this.props.unresolved.data[this.props.match.params.id];

    if (!data) {
      this.props.history.push('/user/unresolved');
      return null;
    }
    return (
      <Card>
        <CardActions style={styles.cardActions}>

          <ConflictButton id={data.id} />
          <ConflictButton approve={false} id={data.id} />

        </CardActions>
        <CardTitle title={fullNameRenderer(data)} subtitle={clubRenderer(data.club)}></CardTitle>

        <SimpleShowLayout record={data} resource="user">

          <FunctionField source="created_by" render={i => i && i.created_by && <Link to={`/user/${i.created_by.id}/show`}>{fullNameRenderer(i.created_by)}</Link>} />

          <TextField source="first_name" />
          <TextField source="last_name" />
          <TextField source="birth_city.name" />
          <TextField source="birth_city.country.name" />

          <TextField source="city.name" />
          <TextField source="city.country.name" />
          <TextField source="address" />
          <TextField source="postal_code" />

          <TextField source="phone" />
          <TextField source="mobile_phone" />

          <EmailField source="email" />
          <TextField source="birthday" />

          <ReferenceField
            reference="personal-document-type"
            source="personal_document_type_id">
            <TextField source="name" />
          </ReferenceField>
          <TextField source="personal_document_number" />
          <TextField source="blood_type" />
          <SelectField source="gender" choices={genderOptions} />

          
          

          <TextField source="pss_member_from" />
          <TextField source="pss_card_number" />
          <TextField source="club.name" />
          <TextField source="club.city.name" />
          <TextField source="club.city.country.name" />

          <FunctionField source="specialties" render={ChipRenderer} />
          <TextField source="date_joined" />
          <TextField source="other_member_legitimacies" />

          <TextField source="id" />

        </SimpleShowLayout>
      </Card>
    );
  }
}

const mapStateToProps = state => ({unresolved: state.admin.resources['user/unresolved']});
export const UserUnresolvedShow = connect(mapStateToProps)(translate(UserUnresolvedShowClass));


const styles = {
  cardActions: {
    zIndex: 2, display: 'inline-block', float: 'right'
  }
}