import React from 'react';
import { Datagrid, TextField } from 'admin-on-rest';


import ReferenceManyField from '../../components/fields/ReferenceManyField';
import { DeleteMembershipFeeButton } from '../../components/buttons/DeleteMembershipFeeButton';


export const MembershipFeesReference = (props) => (
  <ReferenceManyField
    {...props}
    addLabel={false}
    reference={`membership-fee`}
    target="user_id"
    filter={{ club_id: props.record.club_id }}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="membership_fee_for_year" />
      <TextField source="stamp_number" />
      <DeleteMembershipFeeButton redirectPath={`/user/${props.record.id}/show`} />
    </Datagrid>
  </ReferenceManyField>
);