import React, { Component } from 'react';
import { Edit, DisabledInput, FormTab, TextInput, RadioButtonGroupInput, ReferenceInput, SelectInput, ReferenceArrayInput, required, email, ReferenceField, TextField } from 'admin-on-rest';
import TabbedForm from '../../components/forms/TabbedForm';
import DateInput from '../../components/inputs/DateInput';
import { iStyles as styles, cityRenderer, storage } from '../../util';
import CheckboxGroupInput from '../../components/inputs/CustomCheckboxGroupInput';
import AutocompleteInput from '../../components/inputs/AutocompleteInput'
import { rightsOptions, genderOptions } from '../../config';
import { MembershipFeesReference } from './MembershipFeesReference';
import { MembershipToolbar } from './MembershipToolbar';

import { MembershipDialog } from './MembershipDialog';

export class UserEdit extends Component {

  handleOpenMembershipDialog = () => {
    if (this._openMembershipDialog) this._openMembershipDialog();
  }
  handleReloadFees = () => {
    if (this._reloadFees) this._reloadFees();
  }

  render() {
    const isHeadAdmin = parseInt(storage.getItem('rights'), 10) === 2;
    return (
      <Edit {...this.props}>
        <TabbedForm
          submitOnEnter={false}
          redirect={false}>
          <FormTab label="resources.user.tabs.basic">
            
            <DisabledInput
              source="first_name"
              style={styles.inputLeft} />
            <DisabledInput
              source="last_name"
              style={styles.input} />

            <br />


            <ReferenceInput
              validate={required}
              source="birth_city_id"
              reference="city"
              filterToQuery={name => ({ name })}
              style={styles.inputLeft}
              allowEmpty>
              <AutocompleteInput optionText={cityRenderer} addField={false} />
            </ReferenceInput>

            <ReferenceInput
              validate={required}
              source="city_id"
              reference="city"
              filterToQuery={name => ({ name })}
              style={styles.input}
              allowEmpty>
              <AutocompleteInput optionText={cityRenderer} addField={false} />
            </ReferenceInput>
            <br />

            <TextInput
              source="address"
              validate={required}
              style={styles.inputLeft} />
            <TextInput
              source="postal_code"
              style={styles.input} />
            <br />

            <TextInput source="phone" style={styles.inputLeft} />
            <TextInput source="mobile_phone" style={styles.input} />
            <br />

            <TextInput
              source="email"
              validate={email}
              style={styles.inputLeft} />
            <DateInput
              source="birthday"
              validate={required}
              options={{ openToYearSelection: true }}
              style={styles.input} />
            <br />

            
            <ReferenceInput
              source="personal_document_type_id"
              reference="personal-document-type"
              filterToQuery={name => ({ name })}
              style={styles.inputLeft}
              allowEmpty>
              <AutocompleteInput optionText="name" addField={false} />
            </ReferenceInput>
            <TextInput
              source="personal_document_number"
              style={styles.input} />
            <br />

            <TextInput
              source="blood_type"
              style={styles.inputLeft} />
            <RadioButtonGroupInput
              source="gender"
              choices={genderOptions}
              validate={required}
              style={styles.input} />
          </FormTab>

          <FormTab label="resources.user.tabs.pss">
            <DateInput
              source="pss_member_from"
              style={styles.inputLeft} />
            <TextInput
              source="pss_card_number"
              style={styles.input} />
            <br />

            { isHeadAdmin && <ReferenceInput
              validate={required}
              source="club_id"
              reference="club"
              style={styles.inputLeft}
              filterToQuery={name => ({ name })}
              allowEmpty>
              <AutocompleteInput addField={false} />
            </ReferenceInput> }

            { isHeadAdmin && <SelectInput
              source="rights"
              choices={rightsOptions}
              style={styles.input} /> }

            { !isHeadAdmin && <ReferenceField reference="club" source="club_id"><TextField source="name" /></ReferenceField> }

            <br />

            <ReferenceArrayInput source="specialties" reference="specialty" allowEmpty>
              <CheckboxGroupInput optionText="name" />
            </ReferenceArrayInput>

            <br />

            <DateInput
              source="date_joined"
              validate={required}
              style={styles.inputLeft} />
            <TextInput
              source="other_member_legitimacies"
              style={styles.input} />

          </FormTab>

          <FormTab label="resources.user.tabs.membership">
            <MembershipToolbar openDialog={this.handleOpenMembershipDialog} />
            <MembershipFeesReference
              fetchFn={fn => this._reloadFees = fn} />
            <MembershipDialog
              openFn={fn => this._openMembershipDialog = fn}
              reloadFees={this.handleReloadFees} />
          </FormTab>
        </TabbedForm>
      </Edit>
    );
  }
}