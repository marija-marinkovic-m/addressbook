import React, { Component } from 'react';
import { Create, FormTab, TextInput, RadioButtonGroupInput, ReferenceInput, SelectInput, ReferenceArrayInput, CheckboxGroupInput, required, email, FunctionField, translate, DisabledInput } from 'admin-on-rest';

import withWidth from 'material-ui/utils/withWidth';
import { cityRenderer, storage, iStyles as styles } from '../../util';
import { rightsOptions, genderOptions } from '../../config';

import DateInput from '../../components/inputs/DateInput';
import TabbedForm from '../../components/forms/TabbedForm';
import AutocompleteInput from '../../components/inputs/AutocompleteInput'

export class UserCreateClass extends Component {
  render() {
    const isHeadAdmin = parseInt(storage.getItem('rights'), 10) === 2;
    const currentUserClubID = parseInt(storage.getItem('club_id'), 10);
    return (
      <Create {...this.props}>
        <TabbedForm
          submitOnEnter={false}>
          <FormTab label="resources.user.tabs.basic">
            <TextInput
              source="first_name"
              style={styles.inputLeft}
              validate={required} />
            <TextInput
              source="last_name"
              style={styles.input}
              validate={required} />

            <br />

            <ReferenceInput
              validate={required}
              source="birth_city_id"
              reference="city"
              filterToQuery={name => ({name})}
              style={styles.inputLeft}
              allowEmpty>
              <AutocompleteInput optionText={cityRenderer} addField={false} />
            </ReferenceInput>

            <ReferenceInput
              validate={required}
              source="city_id"
              reference="city"
              filterToQuery={name => ({name})}
              style={styles.input}
              allowEmpty>
              <AutocompleteInput optionText={cityRenderer} addField={false} />
            </ReferenceInput>

            <br />
            <TextInput
              source="address"
              validate={required}
              style={styles.inputLeft} />
            <TextInput
              source="postal_code"
              style={styles.input} />
            <br />

            <TextInput source="phone" style={styles.inputLeft} />
            <TextInput source="mobile_phone" style={styles.input} />
            <br />

            <TextInput
              source="email"
              validate={email}
              style={styles.inputLeft} />
            <DateInput
              source="birthday"
              validate={required}
              options={{ openToYearSelection: true }}
              style={styles.input} />
            <br />

            <ReferenceInput
              source="personal_document_type_id"
              reference="personal-document-type"
              filterToQuery={name => ({name})}
              style={styles.inputLeft}
              allowEmpty>
              <AutocompleteInput optionText="name" addField={false} />
            </ReferenceInput>
            <TextInput
              source="personal_document_number"
              style={styles.input} />

            <br />

            <TextInput
              source="blood_type"
              style={styles.inputLeft} />
            <RadioButtonGroupInput
              source="gender"
              choices={genderOptions}
              style={styles.input} />
            <br />

            <TextInput
              source="password"
              style={styles.inputLeft} />
            <TextInput
              source="password_confirmation"
              style={styles.input} />

          </FormTab>

          <FormTab label="resources.user.tabs.pss">

            <DateInput
              source="pss_member_from"
              style={styles.inputLeft} />
            <TextInput
              source="pss_card_number"
              style={styles.input} />
            <br />

            { isHeadAdmin && <ReferenceInput
              source="club_id"
              reference="club"
              style={styles.inputLeft}
              validate={required}
              filterToQuery={name => ({name})}
              allowEmpty>
              <AutocompleteInput addField={false} />
            </ReferenceInput> }

            { isHeadAdmin && <SelectInput
              source="rights"
              choices={rightsOptions}
              style={styles.input} /> }


            { !isHeadAdmin && <DisabledInput style={{display: 'none'}} source="club_id" defaultValue={currentUserClubID} /> }

            <br />

            <ReferenceArrayInput source="specialties" reference="specialty" allowEmpty>
              <CheckboxGroupInput optionText="name" />
            </ReferenceArrayInput>

            <br />

            <DateInput
              source="date_joined"
              validate={required}
              style={styles.inputLeft} />
            <TextInput
              source="other_member_legitimacies"
              style={styles.input} />

          </FormTab>

          <FormTab label="resources.user.tabs.membership">
            <FunctionField label="resources.user.fields.createMembership.label" render={r => this.props.translate('resources.user.fields.createMembership.info')} />
          </FormTab>

        </TabbedForm>
      </Create>
    );
  }
}

export const UserCreate = withWidth()(translate(UserCreateClass));