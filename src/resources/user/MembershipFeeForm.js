import React from 'react';
import { translate } from "admin-on-rest/lib/i18n";
import { Field } from 'redux-form';
import RaisedButton from 'material-ui/RaisedButton';

import { FormTextInput } from '../../components/inputs/FormTextInput';

export const MembershipFeeForm = translate(({ record, onSubmit, onCancel, translate }) => (
  <form onSubmit={onSubmit} style={{ padding: '35px 0' }}>

    <div style={{ display: 'none' }}>
      <Field
        name="user_id"
        readOnly
        disabled
        component={FormTextInput}
        floatingLabelText="user_id"
        initialValue={record.id} />
      <Field
        name="club_id"
        readOnly disabled
        component={FormTextInput}
        floatingLabelText="club_id"
        initialValue={record.club_id} />
    </div>

    <Field
      name="membership_fee_for_year"
      component={FormTextInput}
      floatingLabelText={translate('resources.membership-fee.fields.membership_fee_for_year')}
      style={{ display: 'block' }} />
    <Field
      name="stamp_number"
      component={FormTextInput}
      floatingLabelText={translate('resources.membership-fee.fields.stamp_number')}
      style={{ display: 'block', marginBottom: '40px' }} />


    <RaisedButton
      primary type="submit"
      label={translate('pos.submit')}
      style={{ marginRight: '20px' }} />
    <RaisedButton
      type="button"
      label={translate('aor.action.cancel')}
      onClick={onCancel} />
  </form>
));