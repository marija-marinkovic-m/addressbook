import SocialGroupIcon from 'material-ui/svg-icons/social/group';

export const UserIcon = SocialGroupIcon;
export * from './UserEdit';
export * from './UserList';
export * from './UserShow';
export * from './UserCreate';
export * from './Unresolved';