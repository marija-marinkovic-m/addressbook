import React, { Component } from 'react';
import { Show, Tab, TextField, SelectField, EmailField, RichTextField, FunctionField, ReferenceField } from 'admin-on-rest';
import { rightsOptions, genderOptions } from '../../config';

import TabbedShowLayout from '../../components/fields/TabbedShowLayout';
import { storage } from '../../util/index';

import Chip from 'material-ui/Chip';

import { MembershipDialog } from './MembershipDialog';
import { MembershipFeesReference } from './MembershipFeesReference';
import { MembershipToolbar } from './MembershipToolbar';

export const ChipRenderer = (record, source) => (
  <div style={styles.chipWrapper}>
    {record[source].map((s, i) => <div key={s.id} style={{margin: '0 2.5px'}}><Chip>{s.name}</Chip></div>) }
  </div>
);

export const CategorizationsChipRenderer = (record, source) => (
  <div style={styles.chipWrapper}>
    { record[source].map(item => <Chip key={item.id}><strong>{ item.categorization.name }:</strong>{item.value}, {item.year}</Chip>) }
  </div>
);

export class UserShow extends Component {


  constructor(props) {
    super(props);
    this.state = {
      isRegular: parseInt(storage.getItem('rights'), 10) === 0,
      isSuperAdmin: parseInt(storage.getItem('rights'), 10) > 1
    }
  }

  handleOpenMembershipDialog = () => {
    if (this._openMembershipDialog) this._openMembershipDialog();
  }
  handleReloadFees = () => {
    if (this._reloadFees) this._reloadFees();
  }

  render() {
    const { isRegular, isSuperAdmin } = this.state;
    return (
      <Show {...this.props}>
        <TabbedShowLayout>
          <Tab label="resources.user.tabs.basic">
            <TextField source="first_name" />
            <TextField source="last_name" />
            <TextField source="birth_city.name" />
            <TextField source="birth_city.country.name" />

            <TextField source="city.name" />
            <TextField source="city.country.name" />
            <TextField source="address" />
            <TextField source="postal_code" />

            <TextField source="phone" />
            <TextField source="mobile_phone" />



            <EmailField source="email" />
            <TextField source="birthday" />
            <ReferenceField
              reference="personal-document-type"
              source="personal_document_type_id">
              <TextField source="name" />
            </ReferenceField>
            <TextField source="personal_document_number" />
            <TextField source="blood_type" />
            <SelectField source="gender" choices={genderOptions} />

          </Tab>

          <Tab label="resources.user.tabs.pss">
            <TextField source="pss_member_from" />
            <TextField source="pss_card_number" />
            { isRegular && <TextField source="club.name" /> }
            { !isRegular && <ReferenceField reference="club" source="club_id"><TextField source="name" /></ReferenceField> }
            <TextField source="club.city.name" />
            <TextField source="club.city.country.name" />

            { isSuperAdmin && <SelectField source="rights" choices={rightsOptions} /> }

            <FunctionField source="specialties" render={ChipRenderer} />
            
            <TextField source="date_joined" />

            <RichTextField source="other_member_legitimacies" />

          </Tab>

          { !isRegular && <Tab label="resources.user.tabs.membership">
            <MembershipToolbar openDialog={this.handleOpenMembershipDialog} />
            <MembershipFeesReference
              fetchFn={fn => this._reloadFees = fn} />
            <MembershipDialog
              openFn={fn => this._openMembershipDialog = fn}
              reloadFees={this.handleReloadFees} />
          </Tab> }
        </TabbedShowLayout>
      </Show>
    );
  }
}

const styles = {
  chipWrapper: { display: 'flex', flexWrap: 'wrap', margin: '10px 0' }
}