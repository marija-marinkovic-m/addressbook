import React from 'react';
import { List, Datagrid, TextField, EditButton, Filter, TextInput, ReferenceInput, AutocompleteInput, ReferenceArrayInput, FunctionField } from 'admin-on-rest';
import { storage } from '../../util';
import { ShowLinkField } from '../../components/buttons/ShowResourceLink';

import SelectArrayInput from '../../components/inputs/SelectArrayInput';

export const UserList = (props) => {
  const isRegular = parseInt(storage.getItem('rights'), 10) === 0;
  return (
    <List {...props} filters={<UserFilters />}>
      <Datagrid>
        <TextField source="id" />
        <ShowLinkField source="first_name" />
        <TextField source="email" />
        <FunctionField label="club" render={r => r.club && `${r.club.name}, ${r.club.city.name}, ${r.club.city.country.country_iso_code}`} />
        { !isRegular && <EditButton /> }
      </Datagrid>
    </List>
  );
}


// @NOTE: only head_admin can send club_id
const UserFilters = (props) => {
  const isHeadAdmin = parseInt(storage.getItem('rights'), 10) === 2;
  return (
    <Filter {...props}>
      <TextInput label="pos.search" source="name" alwaysOn />
      <TextInput source="personal_document_number" />
      <TextInput source="pss_card_number" />
      <ReferenceArrayInput
        source="title_id"
        reference="title"
        filterToQuery={searchText => ({ name: searchText })}>
        <SelectArrayInput
          optionText="name"
          addField={false} />
      </ReferenceArrayInput>
      <ReferenceArrayInput
        source="specialty_id"
        filterToQuery={searchText => ({name: searchText})}
        reference="specialty">
        <SelectArrayInput
          optionText="name"
          addField={false} />
      </ReferenceArrayInput>
      <ReferenceArrayInput
        source="mountaineering_course_id"
        reference="mountaineering-course"
        filterToQuery={searchText => ({name: searchText})}
        allowEmpty>
        <SelectArrayInput
          optionText="name" />
      </ReferenceArrayInput>

      { isHeadAdmin && <ReferenceInput
        source="club_id"
        reference="club"
        filterToQuery={searchText => ({name: searchText})}
        allowEmpty>
        <AutocompleteInput addField={false} />
      </ReferenceInput>}
    </Filter>
  );
}