import React from 'react';
import { Datagrid, TextField, ReferenceField, FunctionField } from 'admin-on-rest';
import ReferenceManyField from '../../components/fields/ReferenceManyField';
import { DeleteMembershipFeeButton } from '../../components/buttons/DeleteMembershipFeeButton';
import { fullNameRenderer } from '../../util/index';

export const MembershipFeesReference = props => (
  <ReferenceManyField
    {...props}
    addLabel={false}
    reference="membership-fee"
    target="club_id"
    perPage={10}>
    <Datagrid>
      <TextField source="id" />
      <ReferenceField source="user_id" reference="user">
        <FunctionField render={(r, s) => r && fullNameRenderer(r)} />
      </ReferenceField>
      <TextField source="membership_fee_for_year" />
      <TextField source="stamp_number" />
      <DeleteMembershipFeeButton redirectPath={`/club/${props.record.id}/show`} />
    </Datagrid>
  </ReferenceManyField>
);
