import React, { Component } from 'react';
import { Edit, DisabledInput, FormTab, TextInput, LongTextInput, required } from 'admin-on-rest';

import TabbedForm from '../../components/forms/TabbedForm';
import { EditAutocomplete } from '../../components/inputs/ResourceEditAutocompleteInput';
import { fullNameRenderer, cityRenderer, iStyles } from '../../util/index';
import { MembershipFeesReference } from './MembershipFeesReference';
import { MembershipToolbar } from './MembershipToolbar';
import { MembershipDialog } from './MembershipDialog';

export class ClubEdit extends Component {

  handleOpenMembershipDialog = () => {
    if (this._openMembershipDialog) this._openMembershipDialog();
  }
  handleReloadFees = () => {
    if (this._reloadFees) this._reloadFees();
  }

  render() {
    return (
      <Edit {...this.props}>
        <TabbedForm submitOnEnter={false} redirect={false}>
          <FormTab label="resources.club.tabs.basic">
            <DisabledInput source="id" />

            <TextInput style={iStyles.inputLeft} validate={required} source="name" />

            <EditAutocomplete
              source="contact_person_id"
              reference="user"
              style={iStyles.input}
              allowEmpty
              optionTextRenderer={fullNameRenderer}
              searchTextRenderer={(i) => i.contact_person && `${i.contact_person.first_name} ${i.contact_person.last_name}`} />

            <br />

            <EditAutocomplete
              source="city_id"
              reference="city"
              style={iStyles.inputLeft}
              allowEmpty
              validate={required}
              optionTextRenderer={cityRenderer}
              searchTextRenderer={(i) => `${i.city.name}, ${i.city.country.name}`} />

            <TextInput style={iStyles.input} source="address" />
            <br />

            <TextInput style={iStyles.inputLeft} source="website" />
            <TextInput style={iStyles.input} source="email" />
            <br />

            <TextInput style={iStyles.inputLeft} validate={required} source="phone_number" />
            <TextInput style={iStyles.input} source="year_of_establishment" />
            <br />
            
            <LongTextInput source="description" />


            {/* <NumberInput style={iStyles.input} source="monthly_membership_fee" /> */}
          </FormTab>

          <FormTab label="resources.club.tabs.membership">
            <MembershipToolbar openDialog={this.handleOpenMembershipDialog} />
            <MembershipFeesReference
              fetchFn={fn => this._reloadFees = fn} />
            <MembershipDialog
              openFn={fn => this._openMembershipDialog = fn}
              reloadFees={this.handleReloadFees} />
          </FormTab>
        </TabbedForm>
      </Edit>
    );
  }
}