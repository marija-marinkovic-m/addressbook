import React, { Component } from 'react';
import { translate } from "admin-on-rest/lib/i18n";
import { Field } from 'redux-form';
import RaisedButton from 'material-ui/RaisedButton';

import { FormTextInput } from '../../components/inputs/FormTextInput';
import { restClient } from '../../clients';
import { API_URL } from '../../config';
import { GET_LIST } from 'admin-on-rest/lib/rest/types';
import AutoComplete from 'material-ui/AutoComplete';


class MembershipAutoCompleteInput extends Component {
  state = {
    dataSource: null
  }
  constructor(props) {
    super(props);
    this._rClient = restClient(API_URL);
    this.fetchUsers();
  }
  fetchUsers = () => {
    this._rClient(GET_LIST, 'user', {
      sort: { field: 'id', order: 'DESC' },
      pagination: { page: 1, perPage: 5 },
      filter: { club_id: this.props.clubId }
    })
      .then(res => {
        const dataSource = res.data.map(choice => ({
          value: choice.id,
          text: `${choice.first_name} ${choice.last_name}`,
        }));
        this.setState({ dataSource });
      });
  }
  handleNewRequest = (chosenRequest, index) => {
    if (index < 0 || !chosenRequest.value) return;
    this.props.input.onChange(chosenRequest.value);
  }
  render() {
    const { dataSource } = this.state;
    if (!dataSource) return null;

    const { meta: { touched, error } = {}, input: { ...inputProps }, clubId, ...otherProps } = this.props;
    return (
      <AutoComplete
        errorText={touched && error}
        dataSource={dataSource}
        onNewRequest={this.handleNewRequest}
        openOnFocus
        {...inputProps}
        {...otherProps} />
    );
  }
}

export const MembershipFeeForm = translate(({ record, onSubmit, onCancel, translate }) => (
  <form onSubmit={onSubmit} style={{padding: '20px 0'}}>

    <div style={{ display: 'none' }}>
      <Field
        name="club_id"
        readOnly disabled
        component={FormTextInput}
        initialValue={record.id} />
    </div>

    <Field
      name="user_id"
      component={MembershipAutoCompleteInput}
      floatingLabelText={translate('resources.membership-fee.fields.user_id')}
      style={{ display: 'block' }}
      clubId={record.id} />

    <Field
      name="membership_fee_for_year"
      component={FormTextInput}
      floatingLabelText={translate('resources.membership-fee.fields.membership_fee_for_year')}
      style={{ display: 'block' }} />
    <Field
      name="stamp_number"
      component={FormTextInput}
      floatingLabelText={translate('resources.membership-fee.fields.stamp_number')}
      style={{ display: 'block', marginBottom: '40px' }} />


    <RaisedButton
      primary type="submit"
      label={translate('pos.submit')}
      style={{ marginRight: '20px' }} />
    <RaisedButton
      type="button"
      label={translate('aor.action.cancel')}
      onClick={onCancel} />

  </form>
));