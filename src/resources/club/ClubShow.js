import React, { Component } from 'react';
import { Show, Tab, TextField, RichTextField, FunctionField, EmailField, UrlField } from 'admin-on-rest';

import TabbedShowLayout from '../../components/fields/TabbedShowLayout';
import { storage, cityRenderer } from '../../util';

import { MembershipFeesReference } from './MembershipFeesReference';
import { MembershipToolbar } from './MembershipToolbar';
import { MembershipDialog } from './MembershipDialog';
import { UserReferenceField } from '../../components/fields/UserReferenceField';



export class ClubShow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRegular: parseInt(storage.getItem('rights'), 10) === 0
    }
  }
  handleOpenMembershipDialog = () => {
    if (this._openMembershipDialog) this._openMembershipDialog();
  }
  handleReloadFees = () => {
    if (this._reloadFees) this._reloadFees();
  }
  render() {
    const { isRegular } = this.state;

    return (
      <Show {...this.props}>
        <TabbedShowLayout>
          <Tab label="resources.club.tabs.basic">
            <TextField source="name" />
            <TextField source="phone_number" />
            <TextField source="year_of_establishment" />

            <FunctionField source="city_id" render={record => cityRenderer(record.city)} />

            <TextField source="address" />

            <RichTextField source="description" />

            {/* <TextField source="monthly_membership_fee" /> */}

            <FunctionField source="contact_person_id" render={i => null} />
            <UserReferenceField
              addLabel={false}
              source="contact_person_id"
              reference="user" />

            <UrlField source="website" />
            <EmailField source="email" />
          </Tab>
          {!isRegular && <Tab label="resources.club.tabs.membership">
            <MembershipToolbar openDialog={this.handleOpenMembershipDialog} />
            <MembershipFeesReference
              fetchFn={fn => this._reloadFees = fn} />
            <MembershipDialog
              openFn={fn => this._openMembershipDialog = fn}
              reloadFees={this.handleReloadFees} />
          </Tab>}
        </TabbedShowLayout>
      </Show>
    );
  }
}