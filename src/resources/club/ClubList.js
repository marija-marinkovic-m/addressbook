import React from 'react';
import {
  List, Datagrid, TextField, TextInput,
  EditButton,
  ReferenceInput, translate,
  Filter
} from 'admin-on-rest';
import { ShowLinkField } from '../../components/buttons/ShowResourceLink';

import AutocompleteInput from '../../components/inputs/AutocompleteInput';


export const ClubList = translate(props => (
  <List {...props} filters={<ClubFilters />}>
    <Datagrid>
      <TextField source="id" />
      <ShowLinkField />
      <TextField source="city.name" label={props.translate('City')} />
      <TextField source="address" />
      <TextField source="phone_number" />
      <EditButton />
    </Datagrid>
  </List>
));

const ClubFilters = props => {
  return (
    <Filter {...props}>
      <TextInput label="pos.search" source="name" alwaysOn />
      <TextInput source="year_of_establishment" />
      <ReferenceInput source="city_id" reference="city" filterToQuery={name => ({name})}>
        <AutocompleteInput optionText="name" addField={false} />
      </ReferenceInput>
    </Filter>
  );
}