import React from 'react';
import { translate } from 'admin-on-rest/lib/i18n';
import { Create, FormTab, FunctionField, TextInput, ReferenceInput, LongTextInput, required } from 'admin-on-rest';

import TabbedForm from '../../components/forms/TabbedForm';
import AutocompleteInput from '../../components/inputs/AutocompleteInput';
import { cityRenderer, fullNameRenderer, iStyles } from '../../util';

export const ClubCreate = translate((props) => (
  <Create {...props}>
    <TabbedForm submitOnEnter={false}>
      <FormTab
        label="resources.club.tabs.basic">
        <TextInput
          style={iStyles.inputLeft}
          validate={required}
          source="name" />
        <ReferenceInput
          style={iStyles.input}
          source="contact_person_id"
          reference="user"
          filterToQuery={name => ({ name })}
          allowEmpty>
          <AutocompleteInput
            optionText={fullNameRenderer}
            addField={false} />
        </ReferenceInput>
        <br />

        <ReferenceInput style={iStyles.inputLeft} source="city_id" reference="city" filterToQuery={name => ({ name })} validate={required} allowEmpty>
          <AutocompleteInput optionText={cityRenderer} addField={false} />
        </ReferenceInput>

        <TextInput style={iStyles.input} source="address" />
        <br />

        <TextInput style={iStyles.inputLeft} source="website" />
        <TextInput style={iStyles.input} source="email" />
        <br />

        <TextInput style={iStyles.inputLeft} validate={required} source="phone_number" />
        <TextInput style={iStyles.input} source="year_of_establishment" />
        <br />

        <LongTextInput source="description" />

        {/* <NumberInput style={iStyles.input} source="monthly_membership_fee" /> */}
      </FormTab>
      <FormTab label="resources.club.tabs.membership">
        <FunctionField
          label="resources.user.fields.createMembership.label"
          render={r => props.translate('resources.user.fields.createMembership.info')} />
      </FormTab>
    </TabbedForm>
  </Create>
))