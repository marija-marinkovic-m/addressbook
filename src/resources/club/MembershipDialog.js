import React, { Component } from 'react';
import { CREATE } from 'admin-on-rest/lib/rest/types';
import { restClient } from '../../clients';
import { compose } from 'redux';
import { translate } from 'admin-on-rest/lib/i18n';
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { showNotification as showNotificationAction } from 'admin-on-rest/lib/actions/notificationActions';
import Dialog from 'material-ui/Dialog';

import { MembershipFeeForm } from './MembershipFeeForm';
import { API_URL } from '../../config';


class MembershipDialogClass extends Component {
  state = {
    open: false
  }

  handleOpen = () => this.setState({ open: true })
  handleClose = () => this.setState({ open: false })

  componentDidMount() {
    this._rClient = restClient(API_URL);
    if (this.props.openFn) {
      this.props.openFn(this.handleOpen.bind(this));
    }
    if (this.props.closeFn) {
      this.props.closeFn(this.handleClose.bind(this));
    }
  }

  handleCreateMembership = data => {
    this._rClient(CREATE, 'membership-fee', { data })
      .then(res => {
        if (res) {
          this.props.showNotification('aor.notification.created');
          this.props.reset();
          if (this.props.reloadFees) this.props.reloadFees();
          this.handleClose();
        } else {
          this.props.showNotification('aor.notification.http_error');
        }
      })
      .catch(err => this.props.showNotification('aor.notification.http_error'))
  }

  render() {
    const { open } = this.state;
    const { record, translate, handleSubmit } = this.props;
    const dialogTitle = translate('resources.membership-fee.new');

    return (
      <div>
        <Dialog
          open={open}
          title={dialogTitle}
          onRequestClose={this.handleClose}>
          {
            ((record) => {
              if (!record || !record.id) return;
              return (
                <div>
                  <h3>{translate('resources.club.name', { smart_count: 1 })}: {`${record.name}, ${record.city.name}, ${record.city.country.name}`}</h3>
                  <MembershipFeeForm
                    onSubmit={handleSubmit(this.handleCreateMembership)}
                    onCancel={this.handleClose}
                    record={record} />
                </div>
              );
            })(record)
          }
        </Dialog>
      </div>
    );
  }
}

export const MembershipDialog = compose(
  translate,
  reduxForm({
    form: 'createMembershipFee',
    validate: (values, props) => {
      const errors = {};
      const { translate } = props;
      const required = ['user_id', 'club_id', 'membership_fee_for_year', 'stamp_number'];
      required.forEach(elem => {
        if (!values[elem]) errors[elem] = translate('aor.validation.required')
      });

      return errors;
    },
    keepDirtyOnReinitialize: true
  }),
  connect(null, dispatchEvent => ({
    showNotification: showNotificationAction
  }))
)(MembershipDialogClass);