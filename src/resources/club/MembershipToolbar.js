import React from 'react';

import RaisedButton from 'material-ui/RaisedButton';
import ListIcon from 'material-ui/svg-icons/action/list';

import { Link } from 'react-router-dom';
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar';
import CardMembership from 'material-ui/svg-icons/action/card-membership';
import { translate } from 'admin-on-rest/lib/i18n';


export const MembershipToolbar = translate(({ openDialog, record, translate }) => (
  <Toolbar style={{ marginTop: '40px' }}>
    <ToolbarGroup lastChild={true}>
      {openDialog && <RaisedButton
        onClick={openDialog}
        primary
        label={translate('resources.membership-fee.new')}
        icon={<CardMembership />} />}

      <Link to={`/membership-fee?filter=${encodeURI(JSON.stringify({ club_id: record.id }))}&page=1&perPage=25`}>
        <RaisedButton
          label={translate('resources.membership-fee.nameAll')}
          icon={<ListIcon />} />
      </Link>
    </ToolbarGroup>
  </Toolbar>
));