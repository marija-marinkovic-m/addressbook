import React from 'react';
import { List, Datagrid, TextField, EditButton, Edit, SimpleForm, LongTextInput, DisabledInput, TextInput, required, Create, Show, SimpleShowLayout, RichTextField } from 'admin-on-rest';

import { ExcerptField } from '../../util';
import { ShowLinkField } from '../../components/buttons/ShowResourceLink';

export const CourseList = (props) => (
  <List {...props}>
    <Datagrid>
      <TextField source="id" />
      <ShowLinkField />
      <ExcerptField source="description" maxLength={60} />
      <EditButton />
    </Datagrid>
  </List>
);

export const CourseEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="name" validate={required} />
      <LongTextInput source="description" />
    </SimpleForm>
  </Edit>
);

export const CourseCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" validate={required} />
      <LongTextInput source="description" />
    </SimpleForm>
  </Create>
);

export const CourseShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="name" />
      <RichTextField source="description" />
    </SimpleShowLayout>
  </Show>
);