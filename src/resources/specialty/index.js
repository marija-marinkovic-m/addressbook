import React from 'react';
import { List, Datagrid, TextField, EditButton, Edit, SimpleForm, LongTextInput, DisabledInput, TextInput, required, Create, Show, SimpleShowLayout, RichTextField } from 'admin-on-rest';
import { ExcerptField } from '../../util/index';
import { ShowLinkField } from '../../components/buttons/ShowResourceLink';

export const SpecialtyList = (props) => (
  <List {...props}>
    <Datagrid>
      <TextField source="id" />
      <ShowLinkField source="name" />
      <ExcerptField source="description" maxLength={80} />
      <EditButton />
    </Datagrid>
  </List>
);

export const SpecialtyCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" validate={required} />
      <LongTextInput source="description" />
    </SimpleForm>
  </Create>
);

export const SpecialtyEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <DisabledInput source="code" />
      <TextInput source="name" validate={required} />
      <LongTextInput source="description" />
    </SimpleForm>
  </Edit>
);

export const SpecialtyShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="name" />
      <RichTextField source="description" />
    </SimpleShowLayout>
  </Show>
);
