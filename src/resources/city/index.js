import React from 'react';

import { List, Datagrid, TextField, EditButton, Edit, SimpleForm, DisabledInput, TextInput, Create, ReferenceInput, SelectInput, Show, SimpleShowLayout } from 'admin-on-rest';
import { ShowLinkField } from '../../components/buttons/ShowResourceLink';

export const CityList = (props) => (
  <List {...props}>
    <Datagrid>
      <TextField source="id" />
      <ShowLinkField />
      <TextField source="country.name" />
      <EditButton />
    </Datagrid>
  </List>
);

export const CityEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="name" />
      <ReferenceInput source="country_id" reference="country" allowEmpty>
        <SelectInput />
      </ReferenceInput>
    </SimpleForm>
  </Edit>
);

export const CityCreate = (props) => (
  <Create {...props}>
    <SimpleForm redirect="list">
      <TextInput source="name" />
      <ReferenceInput source="country_id" reference="country" allowEmpty>
        <SelectInput />
      </ReferenceInput>
    </SimpleForm>
  </Create>
);

export const CityShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="name" />
      <TextField source="country.name" />
    </SimpleShowLayout>
  </Show>
);